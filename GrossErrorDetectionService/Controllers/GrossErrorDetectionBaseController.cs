﻿using System.Net.Mime;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GrossErrorDetectionService.Controllers;

/// <summary>
/// Базовый контроллер
/// </summary>
public class GrossErrorDetectionBaseController : ControllerBase
{
    /// <summary>
    /// Возвращает результат в виде JSON объекта из строки
    /// </summary>
    /// <param name="value">Значение</param>
    [ApiExplorerSettings(IgnoreApi = true)]
    public IActionResult JsonFromString(string value)
    {
        return Content(value, MediaTypeNames.Application.Json, Encoding.UTF8);
    }

    /// <summary>
    /// Возвращает результат с внутренней ошибкой сервера
    /// </summary>
    /// <param name="value">Значение</param>
    [ApiExplorerSettings(IgnoreApi = true)]
    public IActionResult InternalError(object value)
    {
        return StatusCode(StatusCodes.Status500InternalServerError, value);
    }
}