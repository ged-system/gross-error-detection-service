using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using GrossErrorDetectionService.Dtos;
using GrossErrorDetectionService.Models;
using GrossErrorDetectionService.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace GrossErrorDetectionService.Controllers;

/// <summary>
/// Контроллер анализа балансовой схемы
/// </summary>
[ApiController]
[Route("api/[controller]/[action]")]
public class AnalysisController : GrossErrorDetectionBaseController
{
    private readonly IBalanceSchemeAnalysisService _balanceSchemeAnalysisService;
    private readonly IMapper _mapper;

    /// <summary>
    /// Контроллер анализа балансовой схемы
    /// </summary>
    /// <param name="mapper">Маппер</param>
    /// <param name="balanceSchemeAnalysisService">Сервис анализа балансовой схемы</param>
    public AnalysisController(IMapper mapper, IBalanceSchemeAnalysisService balanceSchemeAnalysisService)
    {
        _mapper = mapper;
        _balanceSchemeAnalysisService = balanceSchemeAnalysisService;
    }

    /// <summary>
    /// Получение матрицы параметрической чувствительности
    /// </summary>
    /// <param name="requestDtoDto">Объект переноса данных запроса выполнения глобального теста</param>
    /// <returns>Возвращает двумерный массив матрицы параметрической чувствительности</returns>
    [HttpPost]
    [ProducesResponseType(typeof(AnalysisResponseDto), 200)]
    public IActionResult GetParametricSensitivityMatrix([FromBody] AnalysisRequestDto requestDtoDto)
    {
        try
        {
            // Получаем потоки
            var flows = _mapper.Map<IEnumerable<Flow>>(requestDtoDto.Flows);

            // Получаем матрицу параметрической чувствительности
            var result = _balanceSchemeAnalysisService.GetParametricSensitivityMatrix(flows.ToList());

            
            return Ok(JsonConvert.SerializeObject(new AnalysisResponseDto
            {
                Data = result
            }));
        }
        catch (Exception e)
        {
            return InternalError(e.Message);
        }
    }

    /// <summary>
    /// Получение матрицы вкладов измерений в скорректированные значения потоков
    /// </summary>
    /// <param name="requestDto">Объект переноса данных запроса выполнения глобального теста</param>
    /// <returns>Возвращает двумерный массив матрицы параметрической чувствительности</returns>
    [HttpPost]
    [ProducesResponseType(typeof(AnalysisResponseDto), 200)]
    public IActionResult GetContributionsMatrix([FromBody] AnalysisRequestDto requestDto)
    {
        try
        {
            // Получаем потоки
            var flows = _mapper.Map<IEnumerable<Flow>>(requestDto.Flows);

            // Получаем матрицу параметрической чувствительности
            var result = _balanceSchemeAnalysisService.GetContributionsMatrix(flows.ToList());

            return Ok(JsonConvert.SerializeObject(new AnalysisResponseDto
            {
                Data = result
            }));
        }
        catch (Exception e)
        {
            return InternalError(e.Message);
        }
    }

    /// <summary>
    /// Получение матрицы вкладов в дисперсию скорректированных потоков
    /// </summary>
    /// <param name="requestDto">Объект переноса данных запроса выполнения глобального теста</param>
    /// <returns>Возвращает двумерный массив матрицы параметрической чувствительности</returns>
    [HttpPost]
    [ProducesResponseType(typeof(AnalysisResponseDto), 200)]
    public IActionResult GetContributionsToVarianceMatrix([FromBody] AnalysisRequestDto requestDto)
    {
        try
        {
            // Получаем потоки
            var flows = _mapper.Map<IEnumerable<Flow>>(requestDto.Flows);

            // Получаем матрицу параметрической чувствительности
            var result = _balanceSchemeAnalysisService.GetContributionsToVarianceMatrix(flows.ToList());

            return Ok(JsonConvert.SerializeObject(new AnalysisResponseDto
            {
                Data = result
            }));
        }
        catch (Exception e)
        {
            return InternalError(e.Message);
        }
    }
}