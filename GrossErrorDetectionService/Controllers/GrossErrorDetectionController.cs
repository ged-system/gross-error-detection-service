﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using GrossErrorDetectionService.Dtos;
using GrossErrorDetectionService.Enums;
using GrossErrorDetectionService.Models;
using GrossErrorDetectionService.Properties;
using GrossErrorDetectionService.Services;
using Microsoft.AspNetCore.Mvc;

namespace GrossErrorDetectionService.Controllers;

/// <summary>
/// Контроллер поиска грубых ошибок
/// </summary>
[ApiController]
[Route("api/[controller]/[action]")]
public class GrossErrorDetectionController : GrossErrorDetectionBaseController
{
    private readonly IMapper _mapper;
    private readonly IGrossErrorDetectionService _grossErrorDetectionService;

    /// <summary>
    /// Контроллер поиска грубых ошибок
    /// </summary>
    /// <param name="mapper">Маппер</param>
    /// <param name="grossErrorDetectionService">Сервис поиска грубых ошибок</param>
    public GrossErrorDetectionController(IMapper mapper, IGrossErrorDetectionService grossErrorDetectionService)
    {
        _mapper = mapper;
        _grossErrorDetectionService = grossErrorDetectionService;
    }

    /// <summary>
    /// Выполнение глобального теста
    /// </summary>
    /// <param name="requestDto">Объект переноса данных запроса выполнения глобального теста</param>
    /// <returns>Возвращает значение глобального теста для балансовой схемы</returns>
    [HttpPost]
    [ProducesResponseType(typeof(GlobalTestResponseDto), 200)]
    public IActionResult GlobalTest([FromBody] GlobalTestRequestDto requestDto)
    {
        try
        {
            // Получаем потоки
            var flows = _mapper.Map<IEnumerable<Flow>>(requestDto.Flows);

            // Получаем солвер
            var solver = requestDto.Settings.GlobalTestSolver;

            // Выполняем глобальный тест
            var globalTestResult = _grossErrorDetectionService.GlobalTest(flows, solver);

            return Ok(_mapper.Map<GlobalTestResponseDto>(globalTestResult));
        }
        catch (Exception e)
        {
            return InternalError(e.Message);
        }
    }

    /// <summary>
    /// Выполнение поиска грубых ошибок
    /// </summary>
    /// <param name="requestDto">Объект переноса данных запроса поиска грубых ошибок</param>
    /// <returns>Возвращает объект переноса данных результата запроса поиска грубых ошибок</returns>
    [HttpPost]
    [ProducesResponseType(typeof(GedResponseDto), 200)]
    public IActionResult GrossErrorDetection([FromBody] GedRequestDto requestDto)
    {
        try
        {
            // Получаем потоки
            var flows = _mapper.Map<IEnumerable<Flow>>(requestDto.Flows);

            // Получаем солвер
            var solver = requestDto.Settings.GlobalTestSolver;

            // Получаем требуемые ошибки
            var errors = requestDto.Settings.Errors;

            // Выполняем поиск грубых ошибок
            var grossErrorDetectionResult = _grossErrorDetectionService.GrossErrorDetection(flows, solver, errors);

            return Ok(_mapper.Map<GedResponseDto>(grossErrorDetectionResult));
        }
        catch (Exception e)
        {
            return InternalError(e.Message);
        }
    }

    /// <summary>
    /// Выполнение поиска грубых ошибок с применением структуры дерева
    /// </summary>
    /// <param name="requestDto">Объект переноса данных запроса поиска грубых ошибок</param>
    /// <returns>Возвращает объект переноса данных результата запроса поиска грубых ошибок</returns>
    [HttpPost]
    [ProducesResponseType(typeof(IEnumerable<GedResponseDto>), 200)]
    public IActionResult GrossErrorDetectionByTree([FromBody] GedRequestDto requestDto)
    {
        try
        {
            // Получаем потоки
            var flows = _mapper.Map<IEnumerable<Flow>>(requestDto.Flows);

            // Получаем солвер
            var solver = requestDto.Settings.GlobalTestSolver;

            // Получаем требуемые ошибки
            var errors = requestDto.Settings.Errors;

            // Выполняем поиск грубых ошибок
            var grossErrorDetectionResults = _grossErrorDetectionService
                .GrossErrorDetectionByTree(flows, solver, 2, 10, 10, errors);

            return Ok(grossErrorDetectionResults.Select(result => _mapper.Map<GedResponseDto>(result)));
        }
        catch (Exception e)
        {
            return InternalError(e.Message);
        }
    }

    /// <summary>
    /// Выполнение устранения грубых ошибок
    /// </summary>
    /// <param name="requestDto"></param>
    /// <returns>Возвращает объект переноса данных результата устранения грубых ошибок</returns>
    [HttpPost]
    [ProducesResponseType(typeof(IEnumerable<GedReductionResponseDto>), 200)]
    public async Task<IActionResult> GrossErrorDetectionAndReduction([FromBody] GedReductionRequestDto requestDto)
    {
        try
        {
            // Получаем потоки
            var flows = _mapper.Map<IEnumerable<Flow>>(requestDto.Flows).ToList();

            // Получаем солвер глобального теста
            var globalTestSolver = requestDto.Settings.GlobalTestSolver;

            // Получаем солвер сведения баланса
            var balanceSolver = requestDto.Settings.BalanceSolver;

            // Получаем требуемые ошибки
            var errors = requestDto.Settings.Errors;

            var result =
                await _grossErrorDetectionService.GrossErrorDetectionAndReduction(flows, globalTestSolver,
                    balanceSolver, errors);

            return Ok(_mapper.Map<GedReductionResponseDto>(result));
        }
        catch (Exception e)
        {
            return InternalError(e.Message);
        }
    }

    /// <summary>
    /// Выполнение устранения грубых ошибок с применением структуры дерева
    /// </summary>
    /// <param name="requestDto"></param>
    /// <returns>Возвращает объект переноса данных результата устранения грубых ошибок</returns>
    [HttpPost]
    [ProducesResponseType(typeof(IEnumerable<GedReductionResponseDto>), 200)]
    public async Task<IActionResult> GrossErrorDetectionAndReductionByTree([FromBody] GedReductionRequestDto requestDto)
    {
        try
        {
            // Получаем потоки
            var flows = _mapper.Map<IEnumerable<Flow>>(requestDto.Flows).ToList();

            // Получаем солвер глобального теста
            var globalTestSolver = requestDto.Settings.GlobalTestSolver;

            // Получаем солвер сведения баланса
            var balanceSolver = requestDto.Settings.BalanceSolver;

            // Получаем требуемые ошибки
            var errors = requestDto.Settings.Errors;

            var result =
                await _grossErrorDetectionService.GrossErrorDetectionAndReductionByTree(flows, globalTestSolver,
                    balanceSolver, 2, 10, 10, errors);

            return Ok(result.Select(reducedResult => _mapper.Map<GedReductionResponseDto>(reducedResult)));
        }
        catch (Exception e)
        {
            return InternalError(e.Message);
        }
    }


    /// <summary>
    /// Выполнение поиска грубых ошибок в интерактивном режиме
    /// </summary>
    /// <param name="requestDto">Объект переноса данных запроса поиска грубых ошибок в интерактивном режиме</param>
    /// <returns>Возвращает объект переноса данных результата поиска грубых ошибок в интерактивном режиме</returns>
    [HttpPost]
    [ProducesResponseType(typeof(IEnumerable<GedInteractiveResponseDto>), 200)]
    public IActionResult GrossErrorDetectionInteractive([FromBody] GedInteractiveRequestDto requestDto)
    {
        try
        {
            // Получаем балансовую схему
            var flows = requestDto.Flows?.Select(flow => _mapper.Map<Flow>(flow))?.ToList() ?? new List<Flow>();
            if (!flows.Any())
            {
                throw new Exception(ErrorsResources.EmptyFlowsError);
            }

            // Получаем добавленные потоки с ошибками
            var errorFlows = requestDto.ErrorFlows?.Select(flow => _mapper.Map<Flow>(flow)).ToList() ??
                             new List<Flow>();

            // Получаем выбранный поток с ошибкой
            var sampleFlow = _mapper.Map<GlrSampleFlow>(requestDto.SampleFlow);

            var result = _grossErrorDetectionService.GrossErrorDetectionInteractive(flows, errorFlows, sampleFlow,
                GlobalTestSolver.Default, new List<GrossErrorType>
                {
                    GrossErrorType.Measure
                });
            return Ok(_mapper.Map<GedInteractiveResponseDto>(result));
        }
        catch (Exception e)
        {
            return InternalError(e.Message);
        }
    }

    /// <summary>
    /// Выполнение устранения грубых ошибок в интерактивном режиме
    /// </summary>
    /// <param name="requestDto">Объект переноса данных запроса поиска грубых ошибок в интерактивном режиме</param>
    /// <returns>Возвращает объект переноса данных результата устранения грубых ошибок</returns>
    [HttpPost]
    [ProducesResponseType(typeof(IEnumerable<GedReductionResponseDto>), 200)]
    public async Task<IActionResult> GrossErrorReductionInteractive([FromBody] GedInteractiveRequestDto requestDto)
    {
        try
        {
            // Получаем балансовую схему
            var flows = requestDto.Flows?.Select(flow => _mapper.Map<Flow>(flow))?.ToList() ?? new List<Flow>();
            if (!flows.Any())
            {
                throw new Exception(ErrorsResources.EmptyFlowsError);
            }

            // Получаем добавленные потоки с ошибками
            var errorFlows = requestDto.ErrorFlows?.Select(flow => _mapper.Map<Flow>(flow)).ToList() ??
                             new List<Flow>();

            var result = await _grossErrorDetectionService.GrossErrorReductionInteractive(flows, errorFlows,
                GlobalTestSolver.Matlab, BalanceSolver.Matlab, new List<GrossErrorType>
                {
                    GrossErrorType.Measure
                });

            return Ok(_mapper.Map<GedReductionResponseDto>(result));
        }
        catch (Exception e)
        {
            return InternalError(e.Message);
        }
    }
}