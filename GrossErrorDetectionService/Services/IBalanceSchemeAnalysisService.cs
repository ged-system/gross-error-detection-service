using System.Collections.Generic;
using GrossErrorDetectionService.Models;

namespace GrossErrorDetectionService.Services;

/// <summary>
/// Интерфейс сервиса анализа балансовой схемы
/// </summary>
public interface IBalanceSchemeAnalysisService
{
    /// <summary>
    /// Получение матрицы параметрической чувствительности
    /// </summary>
    /// <param name="flows">Коллекция потоков балансовой схемы</param>
    /// <returns>Возвращает двумерный массив представляющий собой матрицу параметрической чувствительности</returns>
    public double[,] GetParametricSensitivityMatrix(ICollection<Flow> flows);
    
    /// <summary>
    /// Получение матрицы вкладов измерений в скорректированные значения потоков.
    /// Матрица параметрической чувствительности представляет собой:
    /// L = B * I * x0
    /// </summary>
    /// <param name="flows">Коллекция потоков балансовой схемы</param>
    /// <returns>Возвращает двумерный массив представляющий собой матрицу</returns>
    public double[,] GetContributionsMatrix(ICollection<Flow> flows);
    
    /// <summary>
    /// Получение матрицы вкладов в дисперсию скорректированных потоков
    /// </summary>
    /// <param name="flows">Коллекция потоков балансовой схемы вкладов в дисперсию скорректированных потоков</param>
    /// <returns>Возвращает двумерный массив представляющий собой матрицу вкладов измерений в скорректированные значения потоков</returns>
    public double[,] GetContributionsToVarianceMatrix(ICollection<Flow> flows);
}