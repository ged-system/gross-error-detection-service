using System;
using System.Collections.Generic;
using System.Linq;
using Accord.Math;
using GrossErrorDetectionService.Models;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace GrossErrorDetectionService.Services;

/// <summary>
/// Сервис анализа балансовой схемы
/// </summary>
public class BalanceSchemeAnalysisService : IBalanceSchemeAnalysisService
{
    /// <summary>
    /// Получение матрицы параметрической чувствительности
    /// Матрица параметрической чувствительности представляет собой:
    /// B = I - Sigma * A† * (A * Sigma * A†)^(-1) * A
    /// </summary>
    /// <param name="flows">Коллекция потоков балансовой схемы</param>
    /// <returns>Возвращает двумерный массив представляющий собой матрицу параметрической чувствительности</returns>
    public double[,] GetParametricSensitivityMatrix(ICollection<Flow> flows)
    {
        // Получаем представление балансовой схемы
        var dataHandler = new DataHandler(flows);

        // Получаем матрица измеряемости
        var i = GetMatrix(dataHandler.I);

        // Получаем матрицу связей
        var a = GetMatrix(dataHandler.Aeq);

        // Получаем транспонированную матрицу связей
        var aTransposed = a.Transpose();

        // Получаем ковариационную матрицу измерений потоков (Sigma = W^(-1))
        var sigma = GetMatrix(dataHandler.W.Inverse());

        return (i - sigma * aTransposed * (a * sigma * aTransposed).Inverse() * a).ToArray();
    }

    /// <summary>
    /// Возвращает матрицу вкладов измерений в скорректированные значения потоков.
    /// Матрица параметрической чувствительности представляет собой:
    /// L = B * I * x0
    /// </summary>
    /// <param name="flows">Коллекция потоков балансовой схемы</param>
    /// <returns>Возвращает двумерный массив представляющий собой матрицу</returns>
    public double[,] GetContributionsMatrix(ICollection<Flow> flows)
    {
        // Получаем представление балансовой схемы
        var dataHandler = new DataHandler(flows);

        // Получаем матрицу параметрической чувствительности
        var b = GetParametricSensitivityMatrix(flows);

        // Получаем вектор измеренных значений
        var measured = dataHandler.X0;

        var l = GetMatrix(b) * GetMatrix(dataHandler.I);

        for (var i = 0; i < b.GetLength(0); i++)
        {
            for (var j = 0; j < b.GetLength(1); j++)
            {
                l[i, j] *= measured[j];
            }
        }

        return l.ToArray();
    }

    /// <summary>
    /// Возвращает матрицу вкладов в дисперсию скорректированных потоков
    /// </summary>
    /// <param name="flows">Коллекция потоков балансовой схемы вкладов в дисперсию скорректированных потоков</param>
    /// <returns>Возвращает двумерный массив представляющий собой матрицу вкладов измерений в скорректированные значения потоков</returns>
    public double[,] GetContributionsToVarianceMatrix(ICollection<Flow> flows)
    {
        // Получаем представление балансовой схемы
        var dataHandler = new DataHandler(flows);

        // Получаем матрицу параметрической чувствительности
        var b = GetParametricSensitivityMatrix(flows);

        // Получаем диагональную матрицу абсолютной погрешности
        var absTolerance = dataHandler.AbsTol;

        var dim1 = b.GetLength(0);
        var dim2 = b.GetLength(1);

        var matrix = GetMatrix(new double[dim1, dim2]);
        var sum = new double[dim1];
        for (var i = 0; i < dim1; i++)
        {
            for (var j = 0; j < dim2; j++)
            {
                matrix[i, j] = Math.Pow(b[i, j], 2) * Math.Pow(absTolerance[j, j], 2);
                sum[i] += matrix[i, j];
            }

            for (var j = 0; j < dim2; j++)
            {
                matrix[i, j] = (matrix[i, j] / sum[i]) * 100;
            }
        }

        return matrix.ToArray();
    }

    /// <summary>
    /// Возвращает плотную или разреженную матрицу
    /// </summary>
    /// <param name="matrix">Двумерный массив для преобразования в матрицу</param>
    /// <returns>Выполняет преобразование двумерного массива в объект матрицы</returns>
    private Matrix<double> GetMatrix(double[,] matrix)
    {
        var countOfZeroes = matrix.Cast<double>().Count(value => value == 0);

        var matrixLength = matrix.Length;
        if ((double) countOfZeroes / matrixLength >= 0.7)
        {
            return SparseMatrix.OfArray(matrix);
        }

        return DenseMatrix.OfArray(matrix);
    }
}