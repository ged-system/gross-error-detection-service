﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GrossErrorDetectionService.Enums;
using GrossErrorDetectionService.Models;

namespace GrossErrorDetectionService.Services;

/// <summary>
/// Интерфейс сервиса поиска грубых ошибок
/// </summary>
public interface IGrossErrorDetectionService
{
    /// <summary>
    /// Вычисление глобального теста
    /// </summary>
    /// <param name="flows">Коллекция потоков балансовой схемы</param>
    /// <param name="solver">Солвер глобального теста</param>
    /// <returns>Объект результата глобального теста</returns>
    public GlobalTestResult GlobalTest(IEnumerable<Flow> flows, GlobalTestSolver solver);

    /// <summary>
    /// Выполнение поиска грубых ошибок
    /// </summary>
    /// <param name="flows">Коллекция потоков балансовой схемы</param>
    /// <param name="solver">Солвер глобального теста</param>
    /// <param name="errorTypes">Типы ошибок, для которых могут создаваться потоки</param>
    /// <returns>Объект результата поиска грубых ошибок</returns>
    public GrossErrorDetectionResult GrossErrorDetection(IEnumerable<Flow> flows, GlobalTestSolver solver,
        ICollection<GrossErrorType> errorTypes);

    /// <summary>
    /// Выполнение поиска грубых ошибок с использованием деревьев
    /// </summary>
    /// <param name="flows">Коллекция потоков балансовой схемы</param>
    /// <param name="solver">Солвер глобального теста</param>
    /// <param name="branchCount">Количество ветвей дерева</param>
    /// <param name="maxTreeHeight">Максимальная высота дерева</param>
    /// <param name="maxSolutionsCount">Максимальное число решений</param>
    /// <param name="errorTypes">Типы ошибок, для которых могут создаваться потоки</param>
    /// <returns>Коллекция объектов результатов поиска грубых ошибок</returns>
    public IEnumerable<GrossErrorDetectionResult> GrossErrorDetectionByTree(IEnumerable<Flow> flows,
        GlobalTestSolver solver,
        int branchCount, int maxTreeHeight, int maxSolutionsCount, ICollection<GrossErrorType> errorTypes);

    /// <summary>
    /// Выполнения поиска и устранения грубых ошибок
    /// </summary>
    /// <param name="flows">Коллекция потоков балансовой схемы</param>
    /// <param name="globalTest">Солвер глобального теста</param>
    /// <param name="balanceSolver">Солвер сведения баланса</param>
    /// <param name="errorTypes">Типы ошибок, для которых могут создаваться потоки</param>
    /// <returns>Объект результата устранения грубых ошибок</returns>
    public Task<GrossErrorDetectionAndReductionResult> GrossErrorDetectionAndReduction(ICollection<Flow> flows,
        GlobalTestSolver globalTest, BalanceSolver balanceSolver, ICollection<GrossErrorType> errorTypes);

    /// <summary>
    /// Выполнения поиска и устранения грубых ошибок с использованием деревьев
    /// </summary>
    /// <param name="flows">Коллекция потоков балансовой схемы</param>
    /// <param name="solver">Солвер глобального теста</param>
    /// <param name="balanceSolver">Солвер сведения баланса</param>
    /// <param name="branchCount">Количество ветвей дерева</param>
    /// <param name="maxTreeHeight">Максимальная высота дерева</param>
    /// <param name="maxSolutionsCount">Максимальное число решений</param>
    /// <param name="errorTypes">Типы ошибок, для которых могут создаваться потоки</param>
    /// <returns>Коллекция объектов результатов поиска грубых ошибок</returns>
    public Task<IEnumerable<GrossErrorDetectionAndReductionResult>> GrossErrorDetectionAndReductionByTree(
        ICollection<Flow> flows,
        GlobalTestSolver solver, BalanceSolver balanceSolver,
        int branchCount, int maxTreeHeight, int maxSolutionsCount, ICollection<GrossErrorType> errorTypes);

    /// <summary>
    /// Выполнение поиска грубых ошибок в интерактивном режиме
    /// </summary>
    /// <param name="flows">Коллекция потоков балансовой схемы</param>
    /// <param name="errorFlows">Коллекция раннее добавленных потоков с ошибками</param>
    /// <param name="sampleFlow">Выбранный поток с ошибкой</param>
    /// <param name="globalTestSolver">Солвер глобального теста</param>
    /// <param name="errorTypes">Типы грубых ошибок, которые требуется обнаружить</param>
    /// <returns>Возвращает объект результата поиска грубых ошибок в интерактивном режиме</returns>
    public GrossErrorDetectionInteractiveResult GrossErrorDetectionInteractive(ICollection<Flow> flows,
        ICollection<Flow> errorFlows, GlrSampleFlow? sampleFlow, GlobalTestSolver globalTestSolver,
        ICollection<GrossErrorType> errorTypes);

    /// <summary>
    /// Выполнения устранения грубых ошибок в интерактивном режиме
    /// </summary>
    /// <param name="flows">Коллекция потоков балансовой схемы</param>
    /// <param name="errorFlows">Коллекция потоков с ошибками</param>
    /// <param name="globalTest">Солвер глобального теста</param>
    /// <param name="balanceSolver">Солвер сведения баланса</param>
    /// <param name="errorTypes">Типы ошибок, для которых могут создаваться потоки</param>
    /// <returns>Объект результата устранения грубых ошибок</returns>
    public Task<GrossErrorDetectionAndReductionResult> GrossErrorReductionInteractive(ICollection<Flow> flows,
        ICollection<Flow> errorFlows, GlobalTestSolver globalTest, BalanceSolver balanceSolver,
        ICollection<GrossErrorType> errorTypes);
}