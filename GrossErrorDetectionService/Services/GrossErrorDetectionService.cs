﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using GrossErrorDetectionService.Dtos;
using GrossErrorDetectionService.Enums;
using GrossErrorDetectionService.Helpers;
using GrossErrorDetectionService.Models;
using GrossErrorDetectionService.Properties;
using GrossErrorDetectionService.SyncDataServices.Http;
using Microsoft.Extensions.DependencyInjection;

namespace GrossErrorDetectionService.Services;

/// <summary>
/// Сервис поиска грубых ошибок
/// </summary>
public class GrossErrorDetectionService : IGrossErrorDetectionService
{
    private readonly IMapper _mapper;
    private readonly IServiceScopeFactory _serviceScopeFactory;

    /// <summary>
    /// Сервис поиска грубых ошибок
    /// </summary>
    /// <param name="serviceScopeFactory">Фабрика сервисов</param>
    /// <param name="mapper">Маппер</param>
    public GrossErrorDetectionService(IServiceScopeFactory serviceScopeFactory, IMapper mapper)
    {
        _serviceScopeFactory = serviceScopeFactory;
        _mapper = mapper;
    }

    /// <summary>
    /// Вычисление глобального теста
    /// </summary>
    /// <param name="flows">Коллекция потоков балансовой схемы</param>
    /// <param name="solver">Солвер глобального теста</param>
    /// <returns>Объект результата глобального теста</returns>
    public GlobalTestResult GlobalTest(IEnumerable<Flow> flows, GlobalTestSolver solver)
    {
        // Получаем представление балансовой схемы
        var dataHandler = new DataHandler(flows.ToList());

        // Получаем матрицу инцидентности
        var matrixAeq = dataHandler.Aeq;

        // Получаем вектор измеренных значений
        var measuredValues = dataHandler.X0;

        // Получаем вектор измеряемости
        var isMeasured = GetDiagonal(dataHandler.I);

        var absoluteTolerance = GetDiagonal(dataHandler.AbsTol);

        // Получаем солвер решения глобального теста
        IGlobalTestSolver globalTestSolver = solver switch
        {
            GlobalTestSolver.Default => new DefaultGlobalTestSolver(),
            GlobalTestSolver.Matlab => new MatlabGlobalTestSolver(),
            _ => new DefaultGlobalTestSolver()
        };

        var stopwatch = new Stopwatch();

        stopwatch.Start();
        // получаем нормированную величину глобального теста
        var value = globalTestSolver.Solve(matrixAeq, measuredValues, isMeasured, absoluteTolerance);
        stopwatch.Stop();

        return new GlobalTestResult()
        {
            Value = value,
            Time = stopwatch.ElapsedMilliseconds
        };
    }

    /// <summary>
    /// Выполнение поиска грубых ошибок
    /// </summary>
    /// <param name="flows">Коллекция потоков балансовой схемы</param>
    /// <param name="solver">Солвер глобального теста</param>
    /// <param name="errorTypes">Типы ошибок, для которых могут создаваться потоки</param>
    /// <returns>Объект результата поиска грубых ошибок</returns>
    public GrossErrorDetectionResult GrossErrorDetection(IEnumerable<Flow> flows, GlobalTestSolver solver,
        ICollection<GrossErrorType> errorTypes)
    {
        // Определяем пороговое значение
        var thresholdValue = 1.0;

        // Определяем сценарий поиска грубых ошибок
        var scenery = new List<Flow>(flows);

        double GlobalTestResult(IEnumerable<Flow> fl, GlobalTestSolver sl) => GlobalTest(fl, sl).Value;
        /*
         * Выполняем глобальный тест для сценария.
         * Пока значение глобального теста превышает или равно пороговому значению
         * продолжаем поиск ошибок.
         */
        var globalTestResult = GlobalTestResult(scenery, solver);

        while (globalTestResult >= thresholdValue)
        {
            // Выполняем GLR-тест
            var glrIterationResults =
                PerformGeneralizedLikelihoodRatioTest(scenery, solver, globalTestResult, errorTypes);

            // Получаем поток, с наивысшей вероятностью содержания грубой ошибки
            var flowWithError = glrIterationResults.FirstOrDefault() ??
                                throw new Exception(ErrorsResources.GrossErrorDetectionError);

            // Получаем новое значение глобального теста
            globalTestResult = flowWithError.GlobalTestResult?.Value ??
                               throw new Exception(ErrorsResources.GrossErrorDetectionError);

            var artificialFlow = flowWithError.Flow ??
                                 throw new Exception(ErrorsResources.GrossErrorDetectionError);

            // Добавляем поток в сценарий
            scenery.Add(artificialFlow);
        }

        return new GrossErrorDetectionResult()
        {
            Flows = new List<Flow>(scenery.Where(flow => flow.Artificial))
        };
    }

    /// <summary>
    /// Выполнение поиска грубых ошибок с использованием деревьев
    /// </summary>
    /// <param name="flows">Коллекция потоков балансовой схемы</param>
    /// <param name="solver">Солвер глобального теста</param>
    /// <param name="branchCount">Количество ветвей дерева</param>
    /// <param name="maxTreeHeight">Максимальная высота дерева</param>
    /// <param name="maxSolutionsCount">Максимальное число решений</param>
    /// <param name="errorTypes">Типы ошибок, для которых могут создаваться потоки</param>
    /// <returns>Коллекция объектов результатов поиска грубых ошибок</returns>
    public IEnumerable<GrossErrorDetectionResult> GrossErrorDetectionByTree(IEnumerable<Flow> flows,
        GlobalTestSolver solver,
        int branchCount, int maxTreeHeight, int maxSolutionsCount, ICollection<GrossErrorType> errorTypes)
    {
        // Определяем пороговое значение
        var thresholdValue = 1.0;

        // Создаём дерево
        var treeStorage = new GedTreeNode<GlrSampleFlow>(null, null, 0);

        // Получаем балансовую схемы
        var scenery = flows.ToList();

        if (!scenery.Any())
        {
            throw new Exception(ErrorsResources.EmptyFlowsError);
        }

        // Выполняем начальный глобальный тест
        var globalTestResult = GlobalTest(scenery, solver).Value;

        // Выполняем поиск грубых ошибок, если значение
        // глобального теста превышает пороговое значение
        if (globalTestResult >= thresholdValue)
        {
            /*
             * 1 этап.
             * При значении глобального теста больше и равного пороговому значению,
             * делает вывод о возможном присутствии в системе грубых ошибок.
             */

            #region 1 step

            // Выполняем GLR-тест и получаем элементы с наивысшей вероятностью содержания ошибки
            var solutions = PerformGeneralizedLikelihoodRatioTest(scenery, solver, globalTestResult, errorTypes)
                .Take(branchCount);

            // К корневому узлу добавляем элементы GLR-теста сценария с наивысшей вероятностью содержания ошибок
            foreach (var solution in solutions)
            {
                treeStorage.AddChild(solution);
            }

            #endregion 1 step

            /*
             * 2 этап.
             * На основании добавленных в дерево узлов строим сценарии,
             * проходя по высотам дерева.
             */

            #region 2 step

            for (var treeHeight = 1; treeHeight < maxTreeHeight; treeHeight++)
            {
                // Прекращаем поиск грубых ошибок, если количество найденных решений
                // превышает заданное количество сценариев
                var solutionsCount = treeStorage.GetLeaves(treeStorage)
                    .Count(x => x.Data?.GlobalTestResult?.Value < thresholdValue);
                if (solutionsCount >= maxSolutionsCount)
                {
                    break;
                }

                // Получаем узлы дерева для заданной высоты
                var treeNodesAtHeight = treeStorage.GetChildrenOfTreeAtHeight(treeHeight);

                // Создаём коллекцию задач
                var tasks = new List<Task>();

                // Создадим задачи по поиску новых сценарий присутствия грубых ошибок
                foreach (var treeNode in treeNodesAtHeight)
                {
                    if (treeNode == null)
                    {
                        throw new Exception(ErrorsResources.GrossErrorDetectionWithTreeError);
                    }

                    // Для каждого узла определяем дочерние узлы, выполняя GLR-тест
                    var task = Task.Run(() =>
                    {
                        // Получаем данные с деерва
                        var treeNodeData = treeNode.Data;
                        if (treeNodeData == null)
                        {
                            throw new Exception(ErrorsResources.GedTreeNodeDataError);
                        }

                        // Получаем поток с наивысшей вероятностью присутствия ошибки
                        var flowWithError = treeNode.Data?.Flow;
                        if (flowWithError == null)
                        {
                            throw new Exception(ErrorsResources.GrossErrorDetectionWithTreeError);
                        }

                        if (treeNodeData.GlobalTestResult?.Value < thresholdValue)
                        {
                            // Если значение глобального теста меньше порогового, то увеличиваем
                            // количество найденных решений и выходим из функции
                            solutionsCount++;
                            return;
                        }

                        // Собираем сценарий, объединяя исходную систему потоков с потоками из узла и его родителей
                        var sceneryFlows = new List<Flow>(scenery);
                        sceneryFlows.AddRange(treeNode.GetParentHierarchy()
                            .Where(x => x.Parent != null && x.Data != null && x.Data.Flow != null)
                            .Select(x =>
                                x.Data?.Flow ?? throw new Exception(ErrorsResources.GrossErrorDetectionWithTreeError)));

                        var treeNodeDataFlow = treeNodeData.Flow;
                        if (treeNodeDataFlow == null)
                        {
                            throw new Exception(ErrorsResources.GrossErrorDetectionWithTreeError);
                        }

                        sceneryFlows.Add(treeNodeDataFlow);

                        // Выполняем GLR-тест и получаем заданное количество элементов
                        // с наивысшей вероятностью содержания ошибки
                        solutions = PerformGeneralizedLikelihoodRatioTest(sceneryFlows, solver, globalTestResult,
                                errorTypes)
                            .Take(branchCount);
                        if (solutions == null)
                        {
                            throw new Exception(ErrorsResources.GrossErrorDetectionWithTreeError);
                        }

                        // Для узла дерева добавляем дочерние элементы
                        foreach (var solution in solutions)
                        {
                            treeNode.AddChild(solution);
                        }
                    });

                    tasks.Add(task);
                }

                // Дожидаемся выполнения задач
                Task.WaitAll(tasks.ToArray());
            }

            #endregion 2 step
        }

        // Получаем листья дерева поиска грубых ошибок
        var leaves = treeStorage.GetLeaves(treeStorage);
        if (leaves == null)
        {
            throw new Exception(ErrorsResources.GrossErrorDetectionWithTreeError);
        }

        // Получаем сценарии
        var sceneries = new List<IEnumerable<GlrSampleFlow>>();
        foreach (var leaf in leaves)
        {
            if (leaf == null)
            {
                throw new Exception(ErrorsResources.GrossErrorDetectionWithTreeError);
            }

            // Пропускаем корневой узел
            if (leaf.Parent == null)
            {
                continue;
            }

            // Пропускаем узел, глобальный тест сценария которого больше или равен порогового значения
            if (leaf.Data?.GlobalTestResult?.Value >= thresholdValue)
            {
                continue;
            }

            var leafData = leaf.Data;
            if (leafData == null)
            {
                throw new Exception(ErrorsResources.GrossErrorDetectionWithTreeError);
            }

            // Собираем сценарий из узлов дерева
            var glrIterationResults = new List<GlrSampleFlow> {leafData};
            var parentNodes = leaf.GetParentHierarchy().Where(node => node.Parent != null).Select(node => node.Data);
            if (parentNodes == null)
            {
                throw new Exception(ErrorsResources.GrossErrorDetectionWithTreeError);
            }

            glrIterationResults.AddRange(parentNodes!);
            sceneries.Add(glrIterationResults);
        }

        // Выполняем группировку решений по количеству ошибок,
        // а затем каждую группу сортируем по увеличению значения глобального теста
        var orderedSceneries = sceneries
            .GroupBy(solution => solution.Count())
            .SelectMany(group => group.OrderBy(x => x.FirstOrDefault()?.GlobalTestResult?.Value)).ToList();

        return orderedSceneries.Select(glrIterationResults => new GrossErrorDetectionResult()
        {
            Flows = glrIterationResults.Select(glrIterationResult =>
                glrIterationResult.Flow ?? throw new Exception(ErrorsResources.GrossErrorDetectionWithTreeError))
        });
    }

    /// <summary>
    /// Выполнения поиска и устранения грубых ошибок
    /// </summary>
    /// <param name="flows">Коллекция потоков балансовой схемы</param>
    /// <param name="globalTest">Солвер глобального теста</param>
    /// <param name="balanceSolver">Солвер сведения баланса</param>
    /// <param name="errorTypes">Типы ошибок, для которых могут создаваться потоки</param>
    public async Task<GrossErrorDetectionAndReductionResult> GrossErrorDetectionAndReduction(ICollection<Flow> flows,
        GlobalTestSolver globalTest, BalanceSolver balanceSolver, ICollection<GrossErrorType> errorTypes)
    {
        // Выполняем поиск грубых ошибок и получаем коллекцию потоков с ошибками
        var grossErrorDetectionResult = GrossErrorDetection(flows, globalTest, errorTypes);

        if (!grossErrorDetectionResult.Flows.Any())
        {
            // Ошибок не найдено
            return new GrossErrorDetectionAndReductionResult();
        }

        // Собираем балансовую схему для устранения грубых ошибок
        var scenery = new List<Flow>(flows);
        foreach (var errorFlow in grossErrorDetectionResult.Flows)
        {
            scenery.Add(errorFlow);
        }

        var requestDto = new BalanceReconciliationRequestDto()
        {
            Flows = scenery.Select(flow => _mapper.Map<FlowDto>(flow)),
            Settings = new BalanceSettingsDto()
            {
                Constraints = BalanceConstraints.Technological,
                Solver = balanceSolver
            }
        };

        // Получаем сервис сведения баланса
        using var scope = _serviceScopeFactory.CreateScope();
        var balanceReconciliationClient = scope.ServiceProvider.GetRequiredService<IBalanceReconciliationClient>();

        var solution = await balanceReconciliationClient.SolveBalanceProblem(requestDto);

        // Получаем потоки
        var solutionFlows = solution.Flows?.Select(flow => _mapper.Map<ReconciledFlow>(flow)).ToList();
        if (solutionFlows == null)
        {
            throw new Exception(ErrorsResources.GrossErrorDetectionAndReductionError);
        }

        // Обрабатываем ответ
        var reducedFlows = HandleReconciledFlows(solutionFlows);

        return new GrossErrorDetectionAndReductionResult()
        {
            ReducedFlows = reducedFlows,
            ImbalanceBefore = solution.ImbalanceBefore,
            ImbalanceAfter = solution.ImbalanceAfter
        };
    }

    /// <summary>
    /// Выполнения поиска и устранения грубых ошибок с использованием деревьев
    /// </summary>
    /// <param name="flows">Коллекция потоков балансовой схемы</param>
    /// <param name="solver">Солвер глобального теста</param>
    /// <param name="balanceSolver">Солвер сведения баланса</param>
    /// <param name="branchCount">Количество ветвей дерева</param>
    /// <param name="maxTreeHeight">Максимальная высота дерева</param>
    /// <param name="maxSolutionsCount">Максимальное число решений</param>
    /// <param name="errorTypes">Типы ошибок, для которых могут создаваться потоки</param>
    /// <returns>Коллекция объектов результатов поиска грубых ошибок</returns>
    public async Task<IEnumerable<GrossErrorDetectionAndReductionResult>> GrossErrorDetectionAndReductionByTree(
        ICollection<Flow> flows,
        GlobalTestSolver solver, BalanceSolver balanceSolver,
        int branchCount, int maxTreeHeight, int maxSolutionsCount, ICollection<GrossErrorType> errorTypes)
    {
        // Получаем сервис сведения баланса
        using var scope = _serviceScopeFactory.CreateScope();
        var balanceReconciliationClient = scope.ServiceProvider.GetRequiredService<IBalanceReconciliationClient>();

        // Получаем сценарии присутствия грубых ошибок
        var grossErrorDetectionResults =
            GrossErrorDetectionByTree(flows, solver, branchCount, maxTreeHeight, maxSolutionsCount, errorTypes)
                .ToList();

        if (!grossErrorDetectionResults.Any())
        {
            // Ошибок не найдено
            return new List<GrossErrorDetectionAndReductionResult>();
        }

        // Объявляем коллекцию потоков с устраненными ошибками
        var result = new List<GrossErrorDetectionAndReductionResult>();

        // Для каждого сценария выполняем устранение грубых ошибок
        foreach (var grossErrorDetectionResult in grossErrorDetectionResults)
        {
            // Собираем балансовую схему для устранения грубых ошибок
            var scenery = new List<Flow>(flows);
            foreach (var errorFlow in grossErrorDetectionResult.Flows)
            {
                scenery.Add(errorFlow);
            }

            var requestDto = new BalanceReconciliationRequestDto()
            {
                Flows = scenery.Select(flow => _mapper.Map<FlowDto>(flow)),
                Settings = new BalanceSettingsDto()
                {
                    Constraints = BalanceConstraints.Technological,
                    Solver = balanceSolver
                }
            };


            var solution = await balanceReconciliationClient.SolveBalanceProblem(requestDto);

            // Получаем потоки
            var solutionFlows = solution.Flows?.Select(flow => _mapper.Map<ReconciledFlow>(flow)).ToList();
            if (solutionFlows == null)
            {
                throw new Exception(ErrorsResources.GrossErrorDetectionAndReductionError);
            }

            // Устраняем ошибки
            var reducedFlows = HandleReconciledFlows(solutionFlows);

            result.Add(new GrossErrorDetectionAndReductionResult()
            {
                ReducedFlows = reducedFlows,
                ImbalanceBefore = solution.ImbalanceBefore,
                ImbalanceAfter = solution.ImbalanceAfter
            });
        }

        return result;
    }

    /// <summary>
    /// Выполнение поиска грубых ошибок в интерактивном режиме
    /// </summary>
    /// <param name="flows">Коллекция потоков балансовой схемы</param>
    /// <param name="errorFlows">Коллекция раннее добавленных потоков с ошибками</param>
    /// <param name="sampleFlow">Выбранный поток с ошибкой</param>
    /// <param name="globalTestSolver">Солвер глобального теста</param>
    /// <param name="errorTypes">Типы грубых ошибок, которые требуется обнаружить</param>
    /// <returns>Возвращает объект результата поиска грубых ошибок в интерактивном режиме</returns>
    public GrossErrorDetectionInteractiveResult GrossErrorDetectionInteractive(ICollection<Flow> flows,
        ICollection<Flow> errorFlows,
        GlrSampleFlow? sampleFlow, GlobalTestSolver globalTestSolver, ICollection<GrossErrorType> errorTypes)
    {
        // Определяем пороговое значение
        var thresholdValue = 1.0;

        // Получаем сценарий
        var scenery = new List<Flow>(flows);

        // Инициализируем значение глобального теста
        var globalTest = 0.0;

        // Проверяем передан ли поток с ошибкой
        if (sampleFlow != null)
        {
            foreach (var errorFlow in errorFlows)
            {
                errorFlow.Artificial = true;
            }

            // При переданном потоке с ошибкой, объединяем все коллекции
            scenery.AddRange(errorFlows);

            // Получаем поток с ошибкой
            var errFlow = sampleFlow.Flow;
            if (errFlow == null)
            {
                throw new Exception(ErrorsResources.GrossErrorDetectionInteractiveErrorFlowIsNullError);
            }

            var globalTestResult = sampleFlow.GlobalTestResult;
            if (globalTestResult == null)
            {
                throw new Exception(ErrorsResources.GrossErrorDetectionInteractiveGlobalTestResultIsNullError);
            }

            // Получаем значением глобального теста для балансовой схемы с потоком
            globalTest = globalTestResult.Value;

            // Добавляем поток в сценарий
            scenery.Add(errFlow);
        }
        else
        {
            // Возвращаем ошибку, если при пустом выбранном потоке, присутствуют потоки с ошибками
            if (errorFlows.Any())
            {
                throw new Exception(ErrorsResources.GrossErrorDetectionInteractiveErrorFlowsMustBeEmptyError);
            }

            // Вычисляем значение глобального теста для балансовой схемы
            var globalTestResult = GlobalTest(scenery, globalTestSolver);

            if (globalTestResult == null)
            {
                throw new Exception(ErrorsResources.GlobalTestSolverError);
            }

            globalTest = globalTestResult.Value;
        }

        // Закончим поиск грубых ошибок, если значение глобального теста меньше порогового значения
        if (globalTest < thresholdValue)
        {
            return new GrossErrorDetectionInteractiveResult()
            {
                Flows = new List<Flow>(flows.ToList()),
                ErrorFlows = scenery.Where(flow => flow.Artificial),
                IsSolved = true
            };
        }

        // Получаем результаты GLR-теста
        var glrSampleFlows =
            PerformGeneralizedLikelihoodRatioTest(scenery, globalTestSolver, globalTest, errorTypes);

        return new GrossErrorDetectionInteractiveResult()
        {
            Flows = new List<Flow>(flows.ToList()),
            ErrorFlows = scenery.Where(flow => flow.Artificial),
            SampleFlows = glrSampleFlows
        };
    }

    /// <summary>
    /// Выполнения устранения грубых ошибок в интерактивном режиме
    /// </summary>
    /// <param name="flows">Коллекция потоков балансовой схемы</param>
    /// <param name="errorFlows">Коллекция потоков с ошибками</param>
    /// <param name="globalTest">Солвер глобального теста</param>
    /// <param name="balanceSolver">Солвер сведения баланса</param>
    /// <param name="errorTypes">Типы ошибок, для которых могут создаваться потоки</param>
    /// <returns>Объект результата устранения грубых ошибок</returns>
    public async Task<GrossErrorDetectionAndReductionResult> GrossErrorReductionInteractive(ICollection<Flow> flows,
        ICollection<Flow> errorFlows, GlobalTestSolver globalTest,
        BalanceSolver balanceSolver, ICollection<GrossErrorType> errorTypes)
    {
        if (!flows.Any())
        {
            throw new Exception(ErrorsResources.GrossErrorReductionInteractiveFlowsError);
        }

        if (!errorFlows.Any())
        {
            throw new Exception(ErrorsResources.GrossErrorReductionInteractiveErrorFlowsError);
        }

        foreach (var errorFlow in errorFlows)
        {
            if (errorFlow.IsMeasured)
            {
                throw new Exception(string.Format(ErrorsResources.GrossErrorReductionInteractiveIsMeasuredError,
                    errorFlow.Id));
            }

            if (!errorFlow.Artificial)
            {
                throw new Exception(string.Format(ErrorsResources.GrossErrorReductionInteractiveArtificialError,
                    errorFlow.Id));
            }
        }

        // Собираем сценарий для устранения грубых ошибок
        var scenery = new List<Flow>(flows);
        scenery.AddRange(errorFlows);

        var requestDto = new BalanceReconciliationRequestDto()
        {
            Flows = scenery.Select(flow => _mapper.Map<FlowDto>(flow)),
            Settings = new BalanceSettingsDto()
            {
                Constraints = BalanceConstraints.Technological,
                Solver = balanceSolver
            }
        };

        // Получаем сервис сведения баланса
        using var scope = _serviceScopeFactory.CreateScope();
        var balanceReconciliationClient = scope.ServiceProvider.GetRequiredService<IBalanceReconciliationClient>();

        var solution = await balanceReconciliationClient.SolveBalanceProblem(requestDto);

        // Получаем потоки
        var solutionFlows = solution.Flows?.Select(flow => _mapper.Map<ReconciledFlow>(flow)).ToList();
        if (solutionFlows == null)
        {
            throw new Exception(ErrorsResources.GrossErrorDetectionAndReductionError);
        }

        // Обрабатываем ответ
        var reducedFlows = HandleReconciledFlows(solutionFlows);

        return new GrossErrorDetectionAndReductionResult()
        {
            ReducedFlows = reducedFlows,
            ImbalanceBefore = solution.ImbalanceBefore,
            ImbalanceAfter = solution.ImbalanceAfter
        };
    }

    /// <summary>
    /// Получить диагональ матрицы
    /// </summary>
    /// <param name="matrix">Двумерный массив, представляющий собой матрицу</param>
    /// <returns>Возвращает диагональ матрицы в виде одномерного массива</returns>
    private double[] GetDiagonal(double[,] matrix)
    {
        if (matrix.GetLength(0) != matrix.GetLength(1))
        {
            throw new Exception(ErrorsResources.GrossErrorDetectionServiceDiagonalError);
        }

        var diagonal = new List<double>();
        for (var i = 0; i < matrix.GetLength(0); i++)
        {
            diagonal.Add(matrix[i, i]);
        }

        return diagonal.ToArray();
    }

    /// <summary>
    /// Выполнение обобщенного теста отношения правдоподобия 
    /// </summary>
    /// <param name="flows">Коллекция потоков</param>
    /// <param name="solver">Солвер глобального теста</param>
    /// <param name="globalTestValue">Значение глобального теста для сценария коллекции потоков</param>
    /// <param name="errorTypes">Типы ошибок, для которых могут создаваться потоки</param>
    /// <param name="async">Показатель выполнения GLR-теста в асинхронном режиме</param>
    /// <returns>Возвращает результат теста в виде коллекции значений GLR-теста отсортированной по убыванию</returns>
    private IEnumerable<GlrSampleFlow> PerformGeneralizedLikelihoodRatioTest(ICollection<Flow> flows,
        GlobalTestSolver solver, double globalTestValue, ICollection<GrossErrorType> errorTypes, bool async = true)
    {
        var artificialFlows = CreateArtificialFlows(flows, errorTypes);

        var glrResult = new List<GlrSampleFlow>();

        // Определяем лямбда-функцию для выполнения итерации GLR-теста.
        var performGlrIteration = (Flow artificialFlow) =>
        {
            var scenery = new List<Flow>(flows);
            if (scenery == null)
            {
                throw new Exception(ErrorsResources.GeneralizedLikelihoodRatioTestError);
            }

            // Получаем сценарий с добавленным потоком
            scenery.Add(artificialFlow);

            // Получаем значение глобального теста для нового сценария
            var globalTest = GlobalTest(scenery, solver);

            // Получаем разницу в значениях исходного глобального теста
            // и глобального теста для нового сценария
            var delta = globalTestValue - globalTest.Value;

            return new GlrSampleFlow()
            {
                Value = delta,
                GlobalTestResult = globalTest,
                Flow = artificialFlow
            };
        };

        // Асинхронное выполнение

        #region async

        if (async)
        {
            // Создаем коллекцию асинхронных задач
            var tasks = new List<Task>();

            // На основе каждого искусственного потока соберем новый сценарий и создадим задачу для
            // нахождения глобального теста сценария
            foreach (var artificialFlow in artificialFlows)
            {
                // Создаем задачу
                tasks.Add(Task.Run(() => performGlrIteration(artificialFlow)));
            }

            // Ожидаем выполнения всех задач
            Task.WaitAll(tasks.ToArray());

            // Получаем коллекцию результатов GLR-тестов для различных сценариев добавления искусственных потоков 
            var glrIterationResults = tasks.Select(task => (task as Task<GlrSampleFlow>)?.Result).ToList();
            if (glrIterationResults == null || glrIterationResults.Any(res => res == null))
            {
                throw new Exception(ErrorsResources.GeneralizedLikelihoodRatioTestError);
            }

            return glrIterationResults.OrderByDescending(glr => glr!.Value)!;
        }

        #endregion

        // Последовательное выполнение

        #region sync

        // На основе каждого искусственного потока соберем новый сценарий и найдем для него глобальный тест
        foreach (var artificialFlow in artificialFlows)
        {
            glrResult.Add(performGlrIteration(artificialFlow));
        }

        #endregion

        // Сортируем результат итераций GLR-теста по убыванию
        return glrResult.OrderByDescending(glr => glr.Value);
    }

    /// <summary>
    /// Создание искусственных потоков для поиска грубых ошибок
    /// </summary>
    /// <param name="flows">Коллекция потоков</param>
    /// <param name="errorTypes">Типы ошибок, для которых могут создаваться потоки</param>
    /// <returns>Метод создает потоки, на основе которых строятся вероятные сценарии присутствия грубых ошибок в системе при GLR-тест</returns>
    private IEnumerable<Flow> CreateArtificialFlows(ICollection<Flow> flows, ICollection<GrossErrorType> errorTypes)
    {
        // Получаем узлы источники
        var sourceNodes = flows.GroupBy(fl => fl.SourceId).Select(gr => gr.Key).ToArray();

        // Получаем узлы назначения
        var destinationNodes = flows.GroupBy(fl => fl.DestinationId).Select(gr => gr.Key).ToArray();

        var artificialFlows = new List<Flow>();

        foreach (var sourceNode in sourceNodes)
        {
            foreach (var destinationNode in destinationNodes)
            {
                // Пропускаем совпадающие потоки
                if (sourceNode == destinationNode)
                {
                    continue;
                }

                // Проверяем, что поток с узлом источником и узлом назначения существует
                var flow = flows.FirstOrDefault(flow =>
                    flow.SourceId == sourceNode && flow.DestinationId == destinationNode);

                // Если поток найден, то создаем его копию и устанавливаем показатель
                if (flow != null && (!errorTypes.Any() || errorTypes.Contains(GrossErrorType.Measure)))
                {
                    // Если поток не учитывается, то пропускаем его
                    if (flow.IsExcluded)
                    {
                        continue;
                    }

                    artificialFlows.Add(new Flow()
                    {
                        Id = flow.Id,
                        Name = flow.Name,
                        SourceId = flow.SourceId,
                        DestinationId = flow.DestinationId,
                        Measured = flow.Measured,
                        Tolerance = flow.Tolerance,
                        IsMeasured = false,
                        IsExcluded = flow.IsExcluded,
                        MetrologicRange = new Constraints()
                        {
                            UpperBound = (flow.MetrologicRange?.UpperBound ?? double.MaxValue) - flow.Measured,
                            LowerBound = (flow.MetrologicRange?.LowerBound ?? 0) - flow.Measured,
                        },
                        TechnologicRange = new Constraints()
                        {
                            UpperBound = (flow.TechnologicRange?.UpperBound ?? double.MaxValue) - flow.Measured,
                            LowerBound = (flow.TechnologicRange?.LowerBound ?? 0) - flow.Measured,
                        },
                        Artificial = true,
                    });

                    continue;
                }

                if (!errorTypes.Contains(GrossErrorType.Leak) && !errorTypes.Contains(GrossErrorType.Unaccounted))
                {
                    continue;
                }

                // Если поток не найден, то создаем новый поток
                var id = Guid.NewGuid().ToString();
                while (flows.Any(f => f.Id == id))
                {
                    id = Guid.NewGuid().ToString();
                }

                artificialFlows.Add(new Flow()
                {
                    Id = id,
                    Name = string.Format(Resources.ArtificialFlow, id),
                    SourceId = sourceNode,
                    DestinationId = destinationNode,
                    Measured = 0.0,
                    Tolerance = 0.0,
                    IsMeasured = false,
                    IsExcluded = false,
                    MetrologicRange = new Constraints()
                    {
                        UpperBound = double.MaxValue,
                        LowerBound = 0.0,
                    },
                    TechnologicRange = new Constraints()
                    {
                        UpperBound = double.MaxValue,
                        LowerBound = 0.0,
                    },
                    Artificial = true,
                });
            }
        }

        return artificialFlows;
    }

    /// <summary>
    /// Выполняет обработку сведенных значений потоков при устранении грубых ошибок
    /// </summary>
    /// <param name="flows">Коллекция сведенных значений потоков</param>
    /// <returns>Возвращает коллекцию с устраненными грубыми ошибками</returns>
    private IEnumerable<ReducedFlow> HandleReconciledFlows(ICollection<ReconciledFlow> flows)
    {
        // Получаем искусственные потоки
        var artificialFlows = flows
            .Where(flow => flow.Artificial)
            .Select(flow => _mapper.Map<ReducedFlow>(flow));

        // Получаем исходные потоки
        var resultedFlows = flows.Where(flow => !flow.Artificial)
            .Select(flow => _mapper.Map<ReducedFlow>(flow))
            .ToList();

        foreach (var artificialFlow in artificialFlows)
        {
            // Проверяем присутствуем ли в коллекции сценария поток идентичный потоку с ошибкой и объединим их
            var identicalFlow = resultedFlows.FirstOrDefault(flow => flow.Id == artificialFlow.Id);

            if (identicalFlow == null)
            {
                // Определяем тип ошибки для потока
                artificialFlow.ErrorType = artificialFlow.DestinationId == null
                    ? GrossErrorType.Leak
                    : GrossErrorType.Unaccounted;

                // Добавляем в результирующую коллекцию искусственный поток, которого раннее не было в системе
                resultedFlows.Add(artificialFlow);
                continue;
            }

            // Если идентичный поток найден, то объединим его с существующим
            var index = resultedFlows.IndexOf(identicalFlow);
            resultedFlows[index].ReconciledMeasured += artificialFlow.ReconciledMeasured;
            resultedFlows[index].ErrorType = GrossErrorType.Measure;
        }

        return resultedFlows;
    }
}