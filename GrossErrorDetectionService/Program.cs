using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text.Json.Serialization;
using GrossErrorDetectionService.Helpers;
using GrossErrorDetectionService.Properties;
using GrossErrorDetectionService.Services;
using GrossErrorDetectionService.SyncDataServices.Http;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System.Linq;

// Добавление сервисов
var builder = WebApplication.CreateBuilder(new WebApplicationOptions
{
    Args = args,
    ApplicationName = typeof(Program).Assembly.FullName,
    ContentRootPath = PathHelpers.RootPath,
    WebRootPath = PathHelpers.WebRootPath
});

#region Добавление Urls в конфигурации

// Проверяем флаг службы
var isService = args.Contains("-service");

// Устанавливаем хост
var host = isService && args.Length >= 3
    ? args[1]
    : string.Empty;

// Устанавливаем порт
var port = isService && args.Length >= 3
    ? args[2]
    : string.Empty;

// Если не в режиме отладки, не сервис и часть конфигурации не заполнена, то спрашиваем параметры у пользователя
if (!Debugger.IsAttached && !isService && (string.IsNullOrWhiteSpace(host) || string.IsNullOrWhiteSpace(port)))
{
    Console.Write(Resources.EnterHost);
    host = Console.ReadLine();
    Console.Write(Resources.EnterPort);
    port = Console.ReadLine();
}

// Если часть параметров осталась не заполнена, то устанавливаем значения по умолчанию
if (string.IsNullOrWhiteSpace(host) || string.IsNullOrWhiteSpace(port))
{
    host = Resources.ApplicationDefaultHost;
    port = Resources.ApplicationDefaultPort;
}

// Добавляем адреса
var urls = $"http://{Resources.ApplicationDefaultHost}:{port}";
if (host != Resources.ApplicationDefaultHost)
{
    urls += $";http://{host}:{port}";
}

// Добавляем Urls в конфиг
builder.WebHost.UseUrls(urls);
#endregion

builder.Services.AddControllers()
    // Добавляем поддержку чтению перечислений в виде строк
    .AddJsonOptions(opt => opt.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()));
builder.Services
    .AddHttpClient<IBalanceReconciliationClient, BalanceReconciliationClient>();
builder.Services
    .AddScoped<IGrossErrorDetectionService, GrossErrorDetectionService.Services.GrossErrorDetectionService>();
builder.Services
    .AddScoped<IBalanceSchemeAnalysisService, BalanceSchemeAnalysisService>();

builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(opt =>
{
    opt.SwaggerDoc("v1", new OpenApiInfo {Title = "Gross Error Detection Service API", Version = "v1"});

    var fileName = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var filePath = Path.Combine(AppContext.BaseDirectory, fileName);
    opt.IncludeXmlComments(filePath);
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
    app.UseSwagger();
    app.UseSwaggerUI(opt =>
    {
        opt.SwaggerEndpoint("v1/swagger.json", "My API V1");
        opt.InjectStylesheet("/Swagger-ui/swagger-ui.css");
    });
}

app.Use(
    async (context, next) =>
    {
        await next().ConfigureAwait(false);

        if (context.Response.StatusCode == StatusCodes.Status404NotFound && !Path.HasExtension(context.Request.Path
                                                                             .Value)
                                                                         && !context.Request.Path.Value.StartsWith(
                                                                             "/api/")
           )
        {
            context.Request.Path = "/index.html";
            await next().ConfigureAwait(false);
        }
    });


// Добавляем обслуживание файлов в корневом каталоге веб-сайта
app.UseDefaultFiles();
app.UseStaticFiles();

app.UseCors(policyBuilder => policyBuilder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());

app.UseRouting();

app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

app.Run();