namespace GrossErrorDetectionService.Dtos;

/// <summary>
/// Объект переноса данных потока с результатом GLR-теста и глобального теста для балансовой схемы с ним
/// </summary>
public class GlrSampleFlowDto
{
    /// <summary>
    /// Значение глобального теста
    /// </summary>
    public double GlobalTestValue { get; set; }

    /// <summary>
    /// Значение GLR-теста
    /// </summary>
    public double GlrValue { get; set; }

    /// <summary>
    /// Объект переноса данных потока
    /// </summary>
    public FlowDto? Flow { get; set; }
}