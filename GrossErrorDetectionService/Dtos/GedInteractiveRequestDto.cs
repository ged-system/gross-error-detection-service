using System.Collections.Generic;

namespace GrossErrorDetectionService.Dtos;

/// <summary>
/// Объект переноса данных запроса поиска грубых ошибок в интерактивном режиме
/// </summary>
public class GedInteractiveRequestDto : BaseBalanceSchemeDto
{
    /// <summary>
    /// Коллекция раннее добавленных потоков с ошибками
    /// </summary>
    public IEnumerable<FlowDto>? ErrorFlows { get; set; }

    /// <summary>
    /// Выбранный поток с ошибкой
    /// </summary>
    public GlrSampleFlowDto? SampleFlow { get; set; }
}