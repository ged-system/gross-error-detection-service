using System.ComponentModel.DataAnnotations;

namespace GrossErrorDetectionService.Dtos;

/// <summary>
/// Объект переноса данных запроса устранения грубых ошибок
/// </summary>
public class GedReductionRequestDto : BaseBalanceSchemeDto
{
    /// <summary>
    /// Объект переноса данных настроек
    /// </summary>
    [Required]
    public SettingsDto Settings { get; set; } = new();
}