﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GrossErrorDetectionService.Enums;

namespace GrossErrorDetectionService.Dtos;

/// <summary>
/// Объект переноса данных настроек
/// </summary>
public class SettingsDto
{
    /// <summary>
    /// Солвер решения глобального теста
    /// </summary>
    public GlobalTestSolver GlobalTestSolver { get; set; } = GlobalTestSolver.Default;

    /// <summary>
    /// Солвер сведения баланса
    /// </summary>
    public BalanceSolver BalanceSolver { get; set; } = BalanceSolver.Default;

    /// <summary>
    /// Типы грубых ошибок
    /// </summary>
    public ICollection<GrossErrorType> Errors { get; set; } = new List<GrossErrorType> {GrossErrorType.Measure};
}