﻿namespace GrossErrorDetectionService.Dtos;

/// <summary>
/// Объект переноса данных выполнения глобального теста
/// </summary>
public class GlobalTestResponseDto
{
    /// <summary>
    /// Значение глобального теста
    /// </summary>
    public double Value { get; set; }

    /// <summary>
    /// Время затраченное на выполнение глобального теста (мс)
    /// </summary>
    public double Time { get; set; }
}