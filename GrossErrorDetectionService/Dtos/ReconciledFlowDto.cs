﻿namespace GrossErrorDetectionService.Dtos;

/// <summary>
/// Объект переноса данных сбалансированного потока
/// </summary>
public class ReconciledFlowDto
{
    /// <summary>
    /// Идентификатор потока
    /// </summary>
    public string? Id { get; set; }

    /// <summary>
    /// Наименование потока
    /// </summary>
    public string? Name { get; set; }

    /// <summary>
    /// Идентификатор узла источника
    /// </summary>
    public string? SourceId { get; set; }

    /// <summary>
    /// Идентификатор узла назначения
    /// </summary>
    public string? DestinationId { get; set; }

    /// <summary>
    /// Измеренное значение потока
    /// </summary>
    public double Measured { get; set; }
    
    /// <summary>
    /// Значение потока после сведения баланса
    /// </summary>
    public double ReconciledMeasured { get; set; }
    
    /// <summary>
    /// Величина изменения между исходным значением потока и сбалансированным
    /// </summary>
    public double DifferenceValue { get; set; }
    
    /// <summary>
    /// Показатель, определяющий, что поток добавлен системой в процессе поиска грубых ошибок
    /// </summary>
    public bool IsArtificial { get; set; }
}