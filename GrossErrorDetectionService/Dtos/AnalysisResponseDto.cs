using System.Collections.Generic;

namespace GrossErrorDetectionService.Dtos;

/// <summary>
/// Объект переноса данных ответа анализа балансовой схемы
/// </summary>
public class AnalysisResponseDto
{
    /// <summary>
    /// Данные анализа в виде двумерной матрицы
    /// </summary>
    public double[,] Data { get; set; }
}