using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GrossErrorDetectionService.Dtos;

/// <summary>
/// Базовый объект переноса данных, содержащий коллекцию потоков балансовой схемы
/// </summary>
public class BaseBalanceSchemeDto
{
    /// <summary>
    /// Коллекция потоков балансовой схемы
    /// </summary>
    [Required]
    public IEnumerable<FlowDto>? Flows { get; set; }
}