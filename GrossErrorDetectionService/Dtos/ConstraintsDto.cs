﻿namespace GrossErrorDetectionService.Dtos;

/// <summary>
/// Объект переноса данных граничных условий
/// </summary>
public class ConstraintsDto
{
    /// <summary>
    /// Верхняя граница
    /// </summary>
    public double UpperBound { get; set; }

    /// <summary>
    /// Нижняя граница
    /// </summary>
    public double LowerBound { get; set; }
}