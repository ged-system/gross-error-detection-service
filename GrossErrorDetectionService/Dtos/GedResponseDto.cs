﻿using System.Collections.Generic;

namespace GrossErrorDetectionService.Dtos;

/// <summary>
/// Объект переноса данных результата поиска грубых ошибок
/// </summary>
public class GedResponseDto
{
    /// <summary>
    /// Коллекция потоков с ошибками
    /// </summary>
    public IEnumerable<FlowErrorDto> Flows { get; set; }
}