using System.Collections.Generic;

namespace GrossErrorDetectionService.Dtos;

/// <summary>
/// Объект переноса данных результата поиска грубых ошибок в интерактивном режиме
/// </summary>
public class GedInteractiveResponseDto
{
    /// <summary>
    /// Коллекция потоков балансовой схемы
    /// </summary>
    public IEnumerable<FlowDto>? Flows { get; set; }

    /// <summary>
    /// Коллекция раннее добавленных потоков с ошибками
    /// </summary>
    public IEnumerable<FlowDto>? ErrorFlows { get; set; }

    /// <summary>
    /// Потоки с найденными ошибками
    /// </summary>
    public IEnumerable<GlrSampleFlowDto>? SampleFlows { get; set; }
    
    /// <summary>
    /// Показатель, определяющий, что решение найдено
    /// </summary>
    public bool IsSolved { get; set; }
}