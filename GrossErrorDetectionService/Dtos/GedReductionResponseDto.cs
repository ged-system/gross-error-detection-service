using System.Collections.Generic;
using System.Linq;

namespace GrossErrorDetectionService.Dtos;

/// <summary>
/// Объект переноса данных результата устранения грубых ошибок
/// </summary>
public class GedReductionResponseDto
{
    /// <summary>
    /// Коллекция объектов переноса данных потоков после устранения грубых ошибок
    /// </summary>
    public IEnumerable<ReducedFlowDto>? ReducedFlows { get; set; }
    
    /// <summary>
    /// Значение дисбаланса до сведения баланса
    /// </summary>
    public double ImbalanceBefore { get; set; }

    /// <summary>
    /// Значение дисбаланса после сведения баланса
    /// </summary>
    public double ImbalanceAfter { get; set; }

    /// <summary>
    /// Количество грубых ошибок
    /// </summary>
    public double ErrorsCount => ReducedFlows?.Count(flow => flow.ErrorType != null) ?? 0;
}