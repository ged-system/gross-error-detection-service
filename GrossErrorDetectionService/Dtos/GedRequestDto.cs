﻿using System.ComponentModel.DataAnnotations;

namespace GrossErrorDetectionService.Dtos;

/// <summary>
/// Объект переноса данных запроса поиска грубых ошибок
/// </summary>
public class GedRequestDto : BaseBalanceSchemeDto
{
    /// <summary>
    /// Объект переноса данных настроек
    /// </summary>
    [Required]
    public SettingsDto Settings { get; set; } = new();
}