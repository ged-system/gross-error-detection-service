using GrossErrorDetectionService.Enums;

namespace GrossErrorDetectionService.Dtos;

/// <summary>
/// Объект переноса данных настроек
/// </summary>
public class BalanceSettingsDto
{
    /// <summary>
    /// Солвер сведения баланса
    /// </summary>
    public BalanceSolver Solver { get; set; }

    /// <summary>
    /// Граничные условия
    /// </summary>
    public BalanceConstraints Constraints { get; set; }
}