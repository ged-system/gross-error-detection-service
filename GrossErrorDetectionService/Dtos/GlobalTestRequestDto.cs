﻿using System.ComponentModel.DataAnnotations;

namespace GrossErrorDetectionService.Dtos;

/// <summary>
/// Объект переноса данных запроса выполнения глобального теста
/// </summary>
public class GlobalTestRequestDto : BaseBalanceSchemeDto
{
    /// <summary>
    /// Объект переноса данных настроек
    /// </summary>
    [Required]
    public SettingsDto Settings { get; set; } = new();
}