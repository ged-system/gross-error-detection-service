﻿namespace GrossErrorDetectionService.Dtos;

/// <summary>
/// Объект переноса данных с ошибкой
/// </summary>
public class FlowErrorDto
{
    /// <summary>
    /// Идентификатор потока
    /// </summary>
    public string? Id { get; set; }

    /// <summary>
    /// Наименование потока
    /// </summary>
    public string? Name { get; set; }

    /// <summary>
    /// Идентификатор узла источника
    /// </summary>
    public string? SourceId { get; set; }

    /// <summary>
    /// Идентификатор узла назначения
    /// </summary>
    public string? DestinationId { get; set; }

    /// <summary>
    /// Измеренное значение потока
    /// </summary>
    public double Measured { get; set; }
    
    /// <summary>
    /// Показатель потока, созданного в процессе поиска грубых ошибок
    /// </summary>
    public bool Artificial { get; set; }
}