﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GrossErrorDetectionService.Enums;

namespace GrossErrorDetectionService.Dtos;

/// <summary>
/// Объект переноса данных запроса сведения баланса
/// </summary>
public class BalanceReconciliationRequestDto : BaseBalanceSchemeDto

{
    /// <summary>
    /// Настройки
    /// </summary>
    [Required]
    public BalanceSettingsDto Settings { get; set; } = new()
    {
        Solver = BalanceSolver.Default,
        Constraints = BalanceConstraints.Technological
    };
}