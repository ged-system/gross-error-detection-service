﻿using System;
using System.Collections.Generic;
using System.Linq;
using GrossErrorDetectionService.Properties;

namespace GrossErrorDetectionService.Models;

/// <summary>
/// Объект представления и обработки балансовой схемы
/// </summary>
public class DataHandler
{
    /// <summary>
    /// Объект представления и обработки балансовой схемы
    /// </summary>
    /// <param name="flows">Коллекция объектов потоков</param>
    public DataHandler(ICollection<Flow> flows)
    {
        // Определяем количество потоков
        var countOfThreads = flows.Count();
        
        // Инициализация матрицы инцидентности ( A )
        Aeq = GetIncidenceMatrix(flows);
        
        // Инициализация вектора измеренных значений ( x0 )
        X0 = new double[countOfThreads];
        
        // Инициализация матрицы измеряемости ( I )
        I = new double[countOfThreads, countOfThreads];
        
        // Инициализация матрицы абсолютной погрешности
        AbsTol = new double[countOfThreads, countOfThreads];
        
        // Инициализация матрицы метрологической погрешности ( 1 / t * t )
        W = new double[countOfThreads, countOfThreads];
        
        for (var i = 0; i < countOfThreads; i++)
        {
            var variable = flows.ElementAt(i);
            
            // Определение вектора измеренных значений
            X0[i] = variable.Measured;

            // Определение матрицы измеряемости
            I[i, i] = variable.IsMeasured ? 1.0 : 0.0;
            
            // Инициализация матрицы абсолютной погрешности
            AbsTol[i, i] = variable.Tolerance;
            
            // Определение матрицы метрологической погрешности
            if (!variable.IsMeasured)
            {
                W[i, i] = 1.0;
            }
            else
            {
                var inaccuracy = variable.Tolerance == 0.0 ? 1.0E-3 : variable.Tolerance;
                W[i, i] = 1.0 / Math.Pow(inaccuracy, 2);
            }
        }
    }
    
    /// <summary>
    /// Матрица инцидентности / связей
    /// </summary>
    public double[,] Aeq { get; }
    
    /// <summary>
    /// Вектор измеренных значений (x0)
    /// </summary>
    public double[] X0 { get; }
        
    /// <summary>
    /// Матрица измеряемости (I)
    /// </summary>
    public double[,] I { get; }
    
    /// <summary>
    /// Матрица абсолютной погрешности
    /// </summary>
    public double[,] AbsTol { get; }
    
    /// <summary>
    /// Матрица метрологической погрешности (W)
    /// </summary>
    public double[,] W { get; }

    /// <summary>
    /// Получение идентификаторов узлов балансовой схемы
    /// </summary>
    /// <param name="flows">Коллекция потоков балансовой схемы</param>
    /// <returns>Возвращает коллекцию уникальных идентификаторов узлов</returns>
    private IEnumerable<string?> GetNodes(ICollection<Flow> flows)
    {
        // Определим узлы балансовой схемы
        var nodes = new List<string?>();

        var sourceNodes = flows.GroupBy(fl => fl.SourceId).Select(gr => gr.Key);
        nodes.AddRange(sourceNodes);

        var destinationNodes = flows.GroupBy(fl => fl.DestinationId).Select(gr => gr.Key);
        nodes.AddRange(destinationNodes);

        // Получаем уникальные узлы, без узла "Окружающая среда" == null
        return nodes.Distinct();
    }
    
    /// <summary>
    /// Получение матрица инцидентности (A) из балансовой схемы 
    /// </summary>
    /// <param name="flows">Коллекция потоков балансовой схемы</param>
    /// <returns>Возвращает двумерный вещественный массив, представляющий собой матрицу инцидентности балансовой схемы</returns>
    private double[,] GetIncidenceMatrix(ICollection<Flow> flows)
    {
        // Получаем уникальные узлы, без узла "Окружающая среда" == null
        var distinctNodes = GetNodes(flows).Distinct().Where(node => node != null);

        // Собираем матрицу инцидентности
        var matrix = distinctNodes.Select(node =>
        {
            return flows.Select(flow =>
            {
                // Проверяем выходит ли поток из узла
                var isSource = flow.SourceId == node;

                // Проверяем входит ли поток в узел
                var isDestination = flow.DestinationId == node;

                // Если поток выходит из узла, устанавливаем значение -1.0
                // Если поток входит в узел, устанавливаем значение 1.0
                return isSource
                    ? -1.0
                    : isDestination
                        ? 1.0
                        : 0.0;
            }).ToArray();
        }).ToArray();

        var rows = matrix.Length;
        var columns = matrix[0].Length;
        if (rows == 0 || columns == 0)
        {
            throw new Exception(ErrorsResources.DataHandlerServicesIncidenceMatrixError);
        }

        var resultedMatrix = new double[rows, columns];
        for (var i = 0; i < rows; i++)
        {
            for (var j = 0; j < columns; j++)
            {
                resultedMatrix[i, j] = matrix[i][j];
            }
        }

        return resultedMatrix;
    }
}