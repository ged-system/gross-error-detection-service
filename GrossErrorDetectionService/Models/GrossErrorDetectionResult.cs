﻿using System.Collections.Generic;

namespace GrossErrorDetectionService.Models
{
    /// <summary>
    /// Объект результат поиска грубых ошибок
    /// </summary>
    public class GrossErrorDetectionResult
    {
        /// <summary>
        /// Коллекция потоков с ошибками
        /// </summary>
        public IEnumerable<Flow> Flows { get; set; } = new List<Flow>();
    }
}
