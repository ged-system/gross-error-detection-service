using GrossErrorDetectionService.Enums;

namespace GrossErrorDetectionService.Models;

/// <summary>
/// Объект обработанного потока после устранения грубых ошибок
/// </summary>
public class ReducedFlow : ReconciledFlow
{
    /// <summary>
    /// Тип ошибки потока
    /// </summary>
    public GrossErrorType? ErrorType { get; set; }
}