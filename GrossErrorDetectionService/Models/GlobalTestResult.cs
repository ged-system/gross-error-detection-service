﻿namespace GrossErrorDetectionService.Models;

/// <summary>
/// Объект результата глобального теста
/// </summary>
public class GlobalTestResult
{
    /// <summary>
    /// Величина глобального теста
    /// </summary>
    public double Value { get; set; }
    
    /// <summary>
    /// Время затраченное на выполнение глобального теста (мс)
    /// </summary>
    public double Time { get; set; }
}