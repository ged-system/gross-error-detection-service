﻿namespace GrossErrorDetectionService.Models;

/// <summary>
/// Объект граничных условий
/// </summary>
public class Constraints
{
    /// <summary>
    /// Верхняя граница
    /// </summary>
    public double UpperBound { get; set; }

    /// <summary>
    /// Нижняя граница
    /// </summary>
    public double LowerBound { get; set; }
}