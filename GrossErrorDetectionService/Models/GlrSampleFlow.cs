﻿namespace GrossErrorDetectionService.Models;

/// <summary>
/// Объект результата итерации GLR-теста
/// </summary>
public class GlrSampleFlow
{
    /// <summary>
    /// Величина GLR-теста
    /// </summary>
    public double Value { get; set; }

    /// <summary>
    /// Результат глобального теста для сценария (балансовой схемы) с добавленным потоком
    /// </summary>
    public GlobalTestResult? GlobalTestResult { get; set; }

    /// <summary>
    /// Поток, добавленный в сценарий
    /// </summary>
    public Flow? Flow { get; set; }
}