using System.Collections.Generic;

namespace GrossErrorDetectionService.Models;

/// <summary>
/// Объект результата устранения грубых ошибок
/// </summary>
public class GrossErrorDetectionAndReductionResult
{
    /// <summary>
    /// Коллекция объектов обработанных потоков после устранения грубых ошибок
    /// </summary>
    public IEnumerable<ReducedFlow> ReducedFlows { get; set; } = new List<ReducedFlow>();

    /// <summary>
    /// Значение дисбаланса до сведения баланса
    /// </summary>
    public double ImbalanceBefore { get; set; }

    /// <summary>
    /// Значение дисбаланса после сведения баланса
    /// </summary>
    public double ImbalanceAfter { get; set; }
}