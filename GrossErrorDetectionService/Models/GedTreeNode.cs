﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using GrossErrorDetectionService.Properties;

namespace GrossErrorDetectionService.Models;

/// <summary>
/// Объект хранения структуры дерева
/// </summary>
/// <typeparam name="T">Тип данных</typeparam>
public class GedTreeNode<T>
{
    // public delegate void TreeVisitor<T>(T nodeData);

    /// <summary>
    /// Объект узла дерева
    /// </summary>
    public T? Data { get; }

    /// <summary>
    ///  Дочерние элементы узла
    /// </summary>
    public BlockingCollection<GedTreeNode<T>> Children { get; }

    /// <summary>
    /// Высота узла
    /// </summary>
    public int Height { get; }

    /// <summary>
    /// Родительский узел
    /// </summary>
    public GedTreeNode<T>? Parent { get; }

    /// <summary>
    /// Объект хранения структуры дерева
    /// </summary>
    /// <param name="data">Объект узла дерева</param>
    /// <param name="parent">Родительский узел</param>
    /// <param name="height">Высота дерева</param>
    public GedTreeNode(T? data, GedTreeNode<T>? parent, int height)
    {
        Data = data;
        Children = new BlockingCollection<GedTreeNode<T>>();
        Parent = parent;
        Height = height;
    }

    /// <summary>
    /// Добавить дочерний узел
    /// </summary>
    /// <param name="data">Объект дочернего узла дерева</param>
    public void AddChild(T data)
    {
        var node = new GedTreeNode<T>(data, this, Height + 1);
        Children.Add(node);
    }

    /// <summary>
    /// Получить дочерний узел
    /// </summary>
    /// <param name="i">Идентификатор узла</param>
    public GedTreeNode<T>? GetChild(int i)
    {
        return Children.ElementAtOrDefault(i);
    }

    /// <summary>
    /// Возвращает дочерние элементы дерева для высоты
    /// </summary>
    /// <param name="height">высота</param>
    public IEnumerable<GedTreeNode<T>> GetChildrenOfTreeAtHeight(int height)
    {
        return GetChildrenOfHeight(GetMainNode(), height);
    }

    /// <summary>
    /// Возвращает "листья" дерева
    /// </summary>
    public IEnumerable<GedTreeNode<T>> GetLeaves(GedTreeNode<T> node)
    {
        var children = new List<GedTreeNode<T>>();

        if (!node.Children.Any())
        {
            children.Add(node);

            return children;
        }

        foreach (var child in node.Children)
        {
            children.AddRange(GetLeaves(child));
        }

        return children;
    }

    /// <summary>
    /// Возвращает корневой узел
    /// </summary>
    public GedTreeNode<T> GetMainNode()
    {
        return GetParentHierarchy().Last();
    }

    /// <summary>
    /// Получаем иерархию родительских узлов.
    /// </summary>
    public ICollection<GedTreeNode<T>> GetParentHierarchy()
    {
        // Получаем корневой узел
        var node = Parent ?? this;

        if (node == null)
        {
            throw new Exception(ErrorsResources.GedTreeNodeParentHierarchyError);
        }
        
        var result = new List<GedTreeNode<T>> {node};

        while (node.Parent != null)
        {
            node = node.Parent!;
            result.Add(node);
        }
        
        return result;
    }

    // public void Traverse(GedTreeNode<T> node, TreeVisitor<T> visitor)
    // {
    //     visitor(node.Data);
    //     foreach (var kid in node.Children)
    //     {
    //         Traverse(kid, visitor);
    //     }
    // }

    // public IEnumerable<T> Flatten()
    // {
    //     return new[] {Data}.Concat(Children.SelectMany(x => x.Flatten()));
    // }

    /// <summary>
    /// Возвращает дочерние элементы узла на заданной высоте
    /// </summary>
    /// <param name="node">Узел дерева</param>
    /// <param name="height">Высота дерева</param>
    private static IEnumerable<GedTreeNode<T>> GetChildrenOfHeight(GedTreeNode<T> node, int height)
    {
        if (height > node.Height)
        {
            return node.Children.SelectMany(x => GetChildrenOfHeight(x, height));
        }

        if (height == node.Height)
        {
            return new List<GedTreeNode<T>> {node};
        }

        return new List<GedTreeNode<T>>();
    }
}