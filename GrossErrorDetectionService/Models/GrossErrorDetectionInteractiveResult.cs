using System.Collections.Generic;

namespace GrossErrorDetectionService.Models;

/// <summary>
/// Объект результата поиска грубых ошибок в интерактивном режиме
/// </summary>
public class GrossErrorDetectionInteractiveResult
{
    /// <summary>
    /// Коллекция потоков балансовой схемы
    /// </summary>
    public IEnumerable<Flow>? Flows { get; set; }

    /// <summary>
    /// Коллекция раннее добавленных потоков с ошибками
    /// </summary>
    public IEnumerable<Flow>? ErrorFlows { get; set; }

    /// <summary>
    /// Потоки с найденными ошибками
    /// </summary>
    public IEnumerable<GlrSampleFlow>? SampleFlows { get; set; }

    /// <summary>
    /// Показатель, определяющий, что решение найдено
    /// </summary>
    public bool IsSolved { get; set; }
}