using AutoMapper;
using GrossErrorDetectionService.Dtos;
using GrossErrorDetectionService.Models;

namespace GrossErrorDetectionService.Profiles;

/// <summary>
/// Профайлер устранения грубых ошибок в интерактивном режиме
/// </summary>
public class GrossErrorReductionInteractiveProfile : Profile
{
    /// <summary>
    /// Профайлер устранения грубых ошибок в интерактивном режиме
    /// </summary>
    public GrossErrorReductionInteractiveProfile()
    {
        // Source -> Target
        CreateMap<FlowDto, Flow>()
            .ForMember(dest => dest.Artificial,
                opt => opt.MapFrom(src => src.IsArtificial));
        CreateMap<Flow, FlowDto>()
            .ForMember(dest => dest.IsArtificial,
                opt => opt.MapFrom(src => src.Artificial));
    }
}