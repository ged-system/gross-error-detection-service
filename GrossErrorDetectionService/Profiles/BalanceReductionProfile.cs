﻿using AutoMapper;
using GrossErrorDetectionService.Dtos;
using GrossErrorDetectionService.Models;

namespace GrossErrorDetectionService.Profiles;

/// <summary>
/// Профайлер объектов сервиса
/// </summary>
public class GrossErrorDetectionProfile : Profile
{
    /// <summary>
    /// Профайлер объектов сервиса
    /// </summary>
    public GrossErrorDetectionProfile()
    {
        // Source -> Target
        CreateMap<FlowDto, Flow>().ForMember(dest => dest.Artificial,
            opt => opt.MapFrom(src => src.IsArtificial));
        CreateMap<ReconciledFlowDto, ReconciledFlow>().ForMember(dest => dest.Artificial,
            opt => opt.MapFrom(src => src.IsArtificial));
        CreateMap<Flow, FlowErrorDto>();
        CreateMap<Flow, FlowDto>().ForMember(dest => dest.IsArtificial,
            opt => opt.MapFrom(src => src.Artificial));
        CreateMap<ConstraintsDto, Constraints>();
        CreateMap<Constraints, ConstraintsDto>();
        CreateMap<GlobalTestResult, GlobalTestResponseDto>();
        CreateMap<GrossErrorDetectionResult, GedResponseDto>();
        CreateMap<ReconciledFlow, ReducedFlow>();
        CreateMap<ReducedFlow, ReducedFlowDto>();
        CreateMap<GrossErrorDetectionAndReductionResult, GedReductionResponseDto>();
    }
}