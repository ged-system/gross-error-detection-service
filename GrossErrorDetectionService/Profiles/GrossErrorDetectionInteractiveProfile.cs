using AutoMapper;
using GrossErrorDetectionService.Dtos;
using GrossErrorDetectionService.Models;

namespace GrossErrorDetectionService.Profiles;

/// <summary>
/// Профайлер поиска грубых ошибок в интерактивном режиме
/// </summary>
public class GrossErrorDetectionInteractiveProfile : Profile
{
    /// <summary>
    /// Профайлер поиска грубых ошибок в интерактивном режиме
    /// </summary>
    public GrossErrorDetectionInteractiveProfile()
    {
        // Source -> Target
        CreateMap<FlowDto, Flow>()
            .ForMember(dest => dest.Artificial,
                opt => opt.MapFrom(src => src.IsArtificial));
        CreateMap<Flow, FlowDto>()
            .ForMember(dest => dest.IsArtificial,
                opt => opt.MapFrom(src => src.Artificial));
        
        CreateMap<GlrSampleFlowDto, GlrSampleFlow>()
            .ForMember(dest => dest.Value,
                opt => opt.MapFrom(src => src.GlrValue))
            .ForMember(dest => dest.GlobalTestResult,
                opt => opt.MapFrom(src => new GlobalTestResult()
                {
                    Value = src.GlobalTestValue
                }));
        CreateMap<GlrSampleFlow, GlrSampleFlowDto>()
            .ForMember(dest => dest.GlrValue,
                opt => opt.MapFrom(src => src.Value))
            .ForMember(dest => dest.GlobalTestValue,
                opt => opt.MapFrom(src => src.GlobalTestResult.Value));
        
        CreateMap<GrossErrorDetectionInteractiveResult, GedInteractiveResponseDto>();
    }
}