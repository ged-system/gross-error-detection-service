using System.Threading.Tasks;
using GrossErrorDetectionService.Dtos;

namespace GrossErrorDetectionService.SyncDataServices.Http;

/// <summary>
/// Интерфейс сервиса взаимодействия с сервисом решения задачи сведения баланса
/// </summary>
public interface IBalanceReconciliationClient
{
    /// <summary>
    /// Выполняет запрос к сервису сведения баланса для решения задачи сведения баланса
    /// </summary>
    /// <param name="requestDto">Объект переноса данных запроса сведения баланса</param>
    /// <returns>Возвращает сведенные значения потоков</returns>
    public Task<BalanceReconciliationResponseDto> SolveBalanceProblem(BalanceReconciliationRequestDto requestDto);
}