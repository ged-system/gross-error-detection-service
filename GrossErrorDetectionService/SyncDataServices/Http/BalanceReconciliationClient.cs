using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using GrossErrorDetectionService.Dtos;
using GrossErrorDetectionService.Properties;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace GrossErrorDetectionService.SyncDataServices.Http;

/// <summary>
/// Сервис взаимодействия с сервисом решения задачи сведения баланса
/// </summary>
public class BalanceReconciliationClient : IBalanceReconciliationClient
{
    private readonly HttpClient _httpClient;
    private readonly IConfiguration _configuration;

    /// <summary>
    /// Сервис взаимодействия с сервисом решения задачи сведения баланса
    /// </summary>
    /// <param name="httpClient">Клиент HTTP</param>
    /// <param name="configuration">Конфигурация</param>
    public BalanceReconciliationClient(HttpClient httpClient, IConfiguration configuration)
    {
        _httpClient = httpClient;
        _configuration = configuration;
    }

    /// <summary>
    /// Выполняет запрос к сервису сведения баланса для решения задачи сведения баланса
    /// </summary>
    /// <param name="requestDto">Объект переноса данных запроса сведения баланса</param>
    /// <returns>Возвращает сведенные значения потоков</returns>
    public async Task<BalanceReconciliationResponseDto> SolveBalanceProblem(BalanceReconciliationRequestDto requestDto)
    {
        var httpContent = new StringContent(
            JsonSerializer.Serialize(requestDto),
            Encoding.UTF8,
            "application/json");

        try
        {
            var response = await _httpClient.PostAsync(
                $"{_configuration[Resources.BalanceReconciliationServicePath]}/{Resources.SolveBalanceProblemPath}",
                httpContent);
            if (!response.IsSuccessStatusCode)
            {
                throw new Exception(ErrorsResources.SolveBalanceProblemError);
            }

            var responseData = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<BalanceReconciliationResponseDto>(responseData);
        }
        catch (Exception e)
        {
            throw new Exception(e.Message);
        }
    }
}