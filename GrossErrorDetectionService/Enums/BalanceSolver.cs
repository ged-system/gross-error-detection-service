﻿namespace GrossErrorDetectionService.Enums;

/// <summary>
/// Перечисление доступных солверов
/// </summary>
public enum BalanceSolver
{
    /// <summary>
    /// Стандартный солвер баланса
    /// </summary>
    Default,
    
    /// <summary>
    /// Солвер баланса матлаб
    /// </summary>
    Matlab
}