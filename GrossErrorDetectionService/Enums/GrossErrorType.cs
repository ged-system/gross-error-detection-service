namespace GrossErrorDetectionService.Enums;

/// <summary>
/// Перечисление типов ошибок
/// </summary>
public enum GrossErrorType
{
    /// <summary>
    /// Ошибка в измерениях
    /// </summary>
    Measure,
    
    /// <summary>
    /// Утечка
    /// </summary>
    Leak,
    
    /// <summary>
    /// Неучтенный поток
    /// </summary>
    Unaccounted
}