﻿namespace GrossErrorDetectionService.Enums;

/// <summary>
/// Солвер глобального теста
/// </summary>
public enum GlobalTestSolver
{
    /// <summary>
    /// Стандартный решение глобального теста
    /// </summary>
    Default,
    
    /// <summary>
    /// Решение глобального теста с помощью Matlab
    /// </summary>
    Matlab
}