﻿using System;
using System.Linq;
using Accord.Math;
using Accord.Statistics.Distributions.Univariate;
using GrossErrorDetectionService.Properties;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using Matrix = Accord.Math.Matrix;

namespace GrossErrorDetectionService.Helpers;

/// <summary>
/// Стандартный солвер решения глобального теста
/// </summary>
public class DefaultGlobalTestSolver : IGlobalTestSolver
{
    /// <summary>
    /// Выполнение глобального теста
    /// </summary>
    /// <param name="matrixAeq">Матрица инцидентности</param>
    /// <param name="measuredValues">Измеренные значения</param>
    /// <param name="isMeasured">Вектор измеряемости</param>
    /// <param name="absoluteTolerance">Вектор абсолютной погрешности</param>
    /// <returns>Возвращает нормированный результат глобального теста (Значение > 1 говорит о присутствии ошибки в системе)</returns>
    public double Solve(double[,] matrixAeq, double[] measuredValues, double[] isMeasured, double[] absoluteTolerance)
    {
        if (absoluteTolerance.Length != isMeasured.Length)
        {
            throw new Exception(ErrorsResources.GlobalTestSolverError);
        }

        // Определяем коэффициент погрешности
        const double coefficientDelta = 1.96;

        // Получаем наибольшее значение потока
        var maxInMeasuredValues = Enumerable.Max(measuredValues);

        /*
         * Введение погрешностей по неизмеряемым потокам
         */
        var xStd = absoluteTolerance.Multiply(1 / coefficientDelta);

        for (var i = 0; i < xStd.Length; i++)
        {
            xStd[i] = isMeasured[i] == 0
                ? 100 * maxInMeasuredValues
                : xStd[i];
        }

        // Получаем квадратный корень
        var xStdSquare = xStd.Select(x => Math.Pow(x, 2)).ToArray();

        // Получаем диагональную матрицу
        var xSigma = Matrix.Diagonal(xStdSquare);

        /*
         *
         */

        var measuredValuesMatrix = new double[measuredValues.Length, 1];
        for (var i = 0; i < measuredValues.Length; i++)
        {
            measuredValuesMatrix[i, 0] = measuredValues[i];
        }

        var aeq = GetMatrix(matrixAeq);
        if (aeq == null)
        {
            throw new Exception(ErrorsResources.GlobalTestSolverError);
        }

        var r = aeq * GetMatrix(measuredValuesMatrix);

        var v = aeq * GetMatrix(xSigma) * aeq.Transpose();

        var aArray = v.ToArray();

        var pinv = Pinv(aArray); // Получаем псевдообратную матрицу

        var gtOriginal = (r.Transpose() * GetMatrix(pinv) * r)[0, 0];

        // Определяем уровень значимости
        const double alpha = 0.05;

        var degreeOfFreedom = matrixAeq.GetLength(0);

        var chiSquared = new ChiSquareDistribution(degreeOfFreedom);

        var gtLimit = chiSquared.InverseDistributionFunction(1 - alpha);

        return gtOriginal == 0 && gtLimit == 0
            ? 0
            // Возвращаем значение нормированного глобального теста
            : gtOriginal / gtLimit;
    }

    /// <summary>
    /// http://forum.alglib.net/viewtopic.php?f=2&t=140
    /// Получение псевдообратной матрицы
    /// </summary>
    /// <param name="inputMatrix">Матрица</param>
    private double[,] Pinv(double[,] inputMatrix)
    {
        var singularValues = new double[inputMatrix.GetLength(1)];
        var u = new double[inputMatrix.GetLength(0), inputMatrix.GetLength(0)];
        var vTranspose = new double[inputMatrix.GetLength(1), inputMatrix.GetLength(1)];
        var singularValuesReciprocal = new double[inputMatrix.GetLength(0), inputMatrix.GetLength(1)];
        var tempMatrix = new double[inputMatrix.GetLength(1), inputMatrix.GetLength(0)];
        var outputMatrix = new double[inputMatrix.GetLength(1), inputMatrix.GetLength(0)];

        alglib.svd.rmatrixsvd(
            inputMatrix, // input
            inputMatrix.GetLength(0), // number of rows
            inputMatrix.GetLength(1), // number of columns
            2, // compute the whole of U
            2, // compute the whole of V_transpose
            2, // use maximum memory for more performance
            ref singularValues, // output singular values, m length vector
            ref u, // output u, m by m matrix
            ref vTranspose, // output v_transpose, n by n matrix
            new alglib.xparams(0));

        // Compute the smallest value that will be considered in computing the reciprocal of r1_singular_values
        // smaller non zero values that should be zero may occur due to numerical precision issues.
        // for example, the reciprocal of 1e-17 would be huge, and 1e-17 is at the limit of double precision.
        var largestSingularValue = singularValues[0];
        var maxDimension = 0;

        if (inputMatrix.GetLength(0) > inputMatrix.GetLength(1))
        {
            maxDimension = inputMatrix.GetLength(0);
        }
        else
        {
            maxDimension = inputMatrix.GetLength(1);
        }

        var machinePrecisionLimit = Math.Pow(1.11 * 10, -16);
        var minimumNonzeroValue = largestSingularValue * maxDimension * machinePrecisionLimit;

        // Compute the matrix composed of reciprocals of the r1_singular_values vector
        for (uint rowIndex = 0; rowIndex < inputMatrix.GetLength(0); rowIndex++)
        {
            for (uint columnIndex = 0; columnIndex < inputMatrix.GetLength(1); columnIndex++)
            {
                if (columnIndex == rowIndex)
                {
                    if (Math.Abs(singularValues[columnIndex]) > minimumNonzeroValue)
                    {
                        singularValuesReciprocal[rowIndex, columnIndex] = 1.0 / singularValues[columnIndex];
                    }
                    else
                    {
                        singularValuesReciprocal[rowIndex, columnIndex] = 0.0;
                    }
                }
                else
                {
                    singularValuesReciprocal[rowIndex, columnIndex] = 0.0;
                }
            }
        }

        // Multiply transpose(r2_v_transpose) and transpose(r2_singular_values_reciprocal)
        alglib.ablas.rmatrixgemm(
            inputMatrix.GetLength(1),
            inputMatrix.GetLength(0),
            inputMatrix.GetLength(1),
            1.0,
            vTranspose, // n x n matrix
            0,
            0,
            1,
            singularValuesReciprocal, // m x n matrix
            0,
            0,
            1, // take the transpose of singular_values_reciprocal
            0,
            tempMatrix, // n x m matrix
            0,
            0,
            new alglib.xparams(0));

        // Multiply temporary_matrix_one and transpose(u)
        alglib.ablas.rmatrixgemm(
            inputMatrix.GetLength(1),
            inputMatrix.GetLength(0),
            inputMatrix.GetLength(0),
            1.0,
            tempMatrix, // n x m matrix
            0,
            0,
            0,
            u, // m x m matrix
            0,
            0,
            1, // take the transpose of u
            0,
            outputMatrix,
            0,
            0,
            new alglib.xparams(0));

        return outputMatrix;
    }

    /// <summary>
    /// Возвращает плотную или разреженную матрицу
    /// </summary>
    /// <param name="matrix">Двумерный массив для преобразования в матрицу</param>
    /// <returns>Выполняет преобразование двумерного массива в объект матрицы</returns>
    private Matrix<double> GetMatrix(double[,] matrix)
    {
        var countOfZeroes = matrix.Cast<double>().Count(value => value == 0);

        var matrixLength = matrix.Length;
        if ((double) countOfZeroes / matrixLength >= 0.7)
        {
            return SparseMatrix.OfArray(matrix);
        }

        return DenseMatrix.OfArray(matrix);
    }
}