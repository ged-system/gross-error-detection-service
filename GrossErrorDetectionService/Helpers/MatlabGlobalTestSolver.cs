﻿using System;
using GrossErrorDetectionService.Properties;
using MathWorks.MATLAB.NET.Arrays;
using MatlabGrossErrorDetectionLibrary;

namespace GrossErrorDetectionService.Helpers;

/// <summary>
/// Солвер решения глобального теста с применением Matlab
/// </summary>
public class MatlabGlobalTestSolver : IGlobalTestSolver
{
    private readonly GrossErrorDetection _solver;

    /// <summary>
    /// Солвер решения глобального теста с применением Matlab
    /// </summary>
    public MatlabGlobalTestSolver()
    {
        _solver = new GrossErrorDetection();
    }

    /// <summary>
    /// Выполнение глобального теста
    /// </summary>
    /// <param name="matrixAeq">Матрица инцидентности</param>
    /// <param name="measuredValues">Измеренные значения</param>
    /// <param name="isMeasured">Вектор измеряемости</param>
    /// <param name="absoluteTolerance">Вектор абсолютной погрешности</param>
    /// <returns>Возвращает нормированный результат глобального теста (Значение > 1 говорит о присутствии ошибки в системе)</returns>
    public double Solve(double[,] matrixAeq, double[] measuredValues, double[] isMeasured, double[] absoluteTolerance)
    {
        // Преобразуем массивы в формат Matlab
        var matrixAeqNumericArray = new MWNumericArray(matrixAeq);
        var vectorMeasuredValuesNumericArray = new MWNumericArray(measuredValues);
        var vectorIsMeasuredNumericArray = new MWNumericArray(isMeasured);
        var vectorAbsoluteToleranceNumericArray = new MWNumericArray(absoluteTolerance);

        // Выполняем глобальный тест
        var data = _solver.GlobalTest(matrixAeqNumericArray, vectorMeasuredValuesNumericArray,
            vectorIsMeasuredNumericArray, vectorAbsoluteToleranceNumericArray);

        if (data == null)
        {
            throw new Exception(ErrorsResources.GlobalTestSolverError);
        }

        // Получаем значение глобального теста
        var result = ((double[,]) data.ToArray())[0, 0];

        return result;
    }
}