﻿using System.Diagnostics;
using System.IO;

namespace GrossErrorDetectionService.Helpers
{
    public static class PathHelpers
    {
        /// <summary>
            /// Путь к исполняемому файлу приложения
            /// </summary>
            public static string ExePath => Process.GetCurrentProcess().MainModule?.FileName ?? string.Empty;

            /// <summary>
            /// Путь к корневой директории приложения
            /// </summary>
            public static string RootPath => Path.GetDirectoryName(ExePath) ?? string.Empty;

            /// <summary>
            /// Путь к корневой директории веб-приложения
            /// </summary>
            public static string WebRootPath
            {
                get
                {
                    var directory = Directory.GetParent(RootPath);

                    return Path.Combine(directory.FullName, "GedAPIUI", "wwwroot");
                }
            }

        /// <summary>
        /// Возвращает полный путь относительно корневой директории приложения
        /// </summary>
        /// <param name="path">Путь</param>
        public static string GetPath(string path)
            {
                return Path.IsPathRooted(path)
                    ? path
                    : Path.GetFullPath(path, RootPath);
            }
    }
}
