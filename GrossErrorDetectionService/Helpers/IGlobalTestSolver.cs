﻿namespace GrossErrorDetectionService.Helpers;

/// <summary>
/// Интерфейс солвера решения глобального теста
/// </summary>
public interface IGlobalTestSolver
{
    /// <summary>
    /// Выполнение глобального теста
    /// </summary>
    /// <param name="matrixAeq">Матрица инцидентности</param>
    /// <param name="measuredValues">Измеренные значения</param>
    /// <param name="isMeasured">Вектор измеряемости</param>
    /// <param name="absoluteTolerance">Вектор абсолютной погрешности</param>
    /// <returns>Возвращает нормированный результат глобального теста (Значение > 1 говорит о присутствии ошибки в системе)</returns>
    public double Solve(double[,] matrixAeq, double[] measuredValues, double[] isMeasured, double[] absoluteTolerance);
}